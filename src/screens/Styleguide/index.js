import React from 'react';
import { Link } from 'react-router-dom';
import Button from '@material-ui/core/Button';

import { H1, H2, H3, H4, H5, H6, P } from '../../components/Typography';

export default (props) => {
  console.log('styleguide', props);
  return (
    <div style={{ padding: 30, display: 'flex' }}>
      <div style={{ flexBasis: '33.33%' }}>
        <H1>Header 1</H1>
        <H2>Header 2</H2>
        <H3>Header 3</H3>
        <H4>Header 4</H4>
        <H5>Header 5</H5>
        <H6>Header 6</H6>
        <P>You might need to access the theme variables inside your React components. Let's say you want to display the value of the primary color, you can use the withTheme() higher-order component to do so. Here is an example:</P>
        <P>The theming solution is very flexible, as you can nest multiple theme providers. This can be really useful when dealing with different area of your application that have distinct appearance from each other.</P>
        
        <hr />
        <Button color="default">Default Button</Button>
        <Button color="secondary">Secondary Button</Button>
        <Button color="primary">Primary Button</Button>
      </div>
      <div style={{ flexBasis: '66.66%' }}>
        <H1>คุณจะทำ PWA ได้ในหนึ่งชั่วโมงได้ยังไง How you can make a progressive web app in an hour</H1>
        <P>In this quick tutorial, I’ll show you how to add your HTML, Javascript, and styling to this template to create an app that works online, offline, in a browser, and as a mobile app</P>
        <P>Progressive web apps are a hybrid of a website and an app that you might use on a tablet or mobile. Making one has a lot in common with creating a website, but with a few key extra files and concepts involved. It need not take a week of trial and error or trawling through a tutorial to get one up and running.</P>
        <P>This post will show you how to make one in an hour that you can then host, use online as a website, and use offline and as a mobile app.</P>
        <P>To give an example of what you could create, here is an example of one I made earlier based on Google’s codelab on web apps. It generates a QR code from the text you enter.</P>
        <Button onClick={() => props.history.push({ pathname: `/map/BTS.S3`, state: { eat: 'burger' }})}>Go to Map</Button>
        <Link to="/contact">Contact</Link>
      </div>
    </div>
  )
}