import React from 'react';
import _ from 'lodash';
import styled from 'styled-components';
import { connect } from 'react-redux';
import numeral from 'numeral';
import { bindActionCreators } from 'redux';

import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Button from '@material-ui/core/Button';
import Popover from '@material-ui/core/Popover';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Checkbox from '@material-ui/core/Checkbox';

import IconLaunch from '@material-ui/icons/Launch';

import Constants from '../../constants/analysis';
import { Circle, OverlayView } from '../../components/Map';

import { filterProjectsByQuery } from './utils';
import { getDistance } from '../../utils/map-utils';
import { getProjectStat } from "../../utils/stat-utils";
import { getMinMaxColorByScoreType, getColorByScoreType } from '../../utils/color-utils';
import { calculateFacilityScore } from '../../utils/facilities-utils';

const OverlayToolbarContainer = styled.div`
  position: absolute;
  right: 30px;
  top: 10px;
  padding: 4px;
  display: flex;
`;

const ToolbarInner = styled.div`
  display: flex;
  background: white;
  border-radius: 4px;
  justify-content: space-between;
`;

const LegendWrapper = styled.div`
  position: absolute;
  left: 50%;
  transform: translateX(-50%);
  
  bottom: 10px;
  padding: 12px 4px 4px;
  display: flex;
  flex-direction: row;
  align-items: flex-end;
  background: white;

  font-size: 10px;
`;

const LegendBar = styled.div`

  height: 10px;
  width: 200px;
  margin: 0 10px;
  background: linear-gradient(to right, ${props => props.min} 0%, ${props => props.max} 100%);
`;

const StationInfo = styled.div`
  background: transparent;
  color: white;
  width: 40;
  height: 40;
  text-align: center;
  border-radius: 40;
  line-height: 1.4;

  .popover {
    position: absolute;
    top: 0;
    left: 50%;
    z-index: 1000;

    .popover-inner {
      width: 180px;
      background: rgba(0, 0, 0, 0.8);
      color: white;
      transform: translate(-50%, -100%);
      table { width: 100%; }
      tr { line-height: 1.4; }
      td { padding: 3px };
    }
  }
`;

const getProjectWithinRadius = (projects, { lat, lng }, inner, outer) => {
  return _.filter(projects, p => {
    // console.log('p', p);
    const projectLat = _.get(p, 'location.position._lat');
    const projectLng = _.get(p, 'location.position._long');
    const d = getDistance(projectLat, projectLng, lat, lng);
    // console.log(projectLat, projectLng, lat, lng, d);
    return d <= 0.001 * outer && d >= 0.001 * inner;
  });
};

const getProjectsStats = (projects) => {
  
  const allProjectStats = _.map(projects, getProjectStat);
 
  const capitalGainValues = _.compact(_.map(allProjectStats, stat => stat.capitalGain));
  const rentPriceValues = _.compact(_.map(allProjectStats, stat => stat.rentPrice));
  const rentalYieldValues = _.compact(_.map(allProjectStats, stat => stat.rentalYield));
  const salePriceValues = _.compact(_.map(allProjectStats, stat => stat.salePrice));
  const startingPriceValues = _.compact(_.map(allProjectStats, stat => stat.startingPrice));

  return {
    salePrice: _.sum(salePriceValues) / _.size(salePriceValues),
    capitalGain: _.sum(capitalGainValues) / _.size(capitalGainValues),
    startingPrice: _.sum(startingPriceValues) / _.size(startingPriceValues),
    rentPrice: _.sum(rentPriceValues) / _.size(rentPriceValues),
    rentalYield: _.sum(rentalYieldValues) / _.size(rentalYieldValues)
  };
}


const getCircleLabelPixelPositionOffset = (width, height) => ({
  x: -(width / 2),
  y: -(height / 2),
});

//   const stations = _.filter(_.get(state, 'entities.pointOfInterests.entities'), poi => _.includes(['BTS', 'MRT'], poi.type));

const StationCircle = connect(
  (state, ownProps) => ({
    hilight: _.includes(_.get(state, 'domain.map.analysisHilightStations'), ownProps.id),
    hasHilight: !_.isEmpty(_.get(state, 'domain.map.analysisHilightStations')),
    radius: _.get(state, 'domain.map.analysisStationRadius', 600),
    title: _.get(state, ['entities', 'pointOfInterests', 'entities', ownProps.id, 'title', 'en']),
  }),
  (dispatch) => bindActionCreators({
    toggleHilightStation: stationId => ({ type: 'DOMAIN/MAP/ANALYSIS/HILIGHT_STATION/TOGGLE', stationId }),
    hilightStation: stationId => ({ type: 'DOMAIN/MAP/ANALYSIS/HILIGHT_STATION/CHANGE', stationId }),
    clearHilightStation: () => ({ type: 'DOMAIN/MAP/ANALYSIS/HILIGHT_STATION/CLEAR' })
  }, dispatch)
)(class extends React.Component {
  shouldComponentUpdate (nextProps) {
    return !_.isEqual(nextProps, this.props);
  }

  render() {
    const { id, lat, lng, color, view, stats, hasHilight, hilight, radius, title, toggleHilightStation } = this.props;
    return (
      <React.Fragment>
        <Circle
          key={id}
          center={{ lat, lng }}
          radius={radius}
          options={{
            fillColor: color,
            fillOpacity: hasHilight ? (hilight ? 1 : 0.2) : 0.5,
            strokeColor: color,
            strokeOpacity: hasHilight ? (hilight ? 1 : 0.4) : 0.7,
            strokeWeight: 2,
          }}
          onClick={e => toggleHilightStation(id)}
        />
        <OverlayView
          position={{ lat, lng }}
          mapPaneName={OverlayView.OVERLAY_MOUSE_TARGET}
          getPixelPositionOffset={getCircleLabelPixelPositionOffset}
        >
          <StationInfo>
            <div style={{ opacity: hasHilight ? (hilight ? 1 : 0.2) : 1 }}>
              <div>{title}</div>
              <div>{`${_.get(Constants, `${view}.prefix`)}${numeral(_.get(stats, view)).format(_.get(Constants, `${view}.format`))}`}</div>
            </div>
            {hilight &&
              <div className="popover">
                <div className="popover-inner">
                  <table>
                    <tbody>
                      <tr><td colSpan={2}>{title}</td></tr>
                      {_.map(Constants, ({ label, prefix, format }, key) => (
                        <tr key={key}><td>{label}</td><td>{`${prefix}${numeral(_.get(stats, key)).format(format)}`}</td></tr>
                      ))}
                    </tbody>
                  </table>
                </div>
              </div>
            }
          </StationInfo>
        </OverlayView>
      </React.Fragment>
    );
  }
});

const mapStateToProps = (state, ownProps) => {
  const mapReducer = _.get(state, 'domain.map');
  const analysisReducer = _.get(state, 'domain.analysis');
  const { query } = mapReducer;

  const radius = mapReducer.analysisStationRadius || 600;

  const stations = _.filter(_.get(state, 'entities.pointOfInterests.entities'), poi => _.includes(['BTS', 'MRT'], poi.type));
  const visibleStations = _.filter(stations, st => _.size(_.intersection(st.lines, analysisReducer.viewOptionsLines)) > 0);
  const projectEntities = _.get(state, 'entities.projects.entities');
  const filteredProjects = filterProjectsByQuery(projectEntities, query);
  const allFacilities = _.get(state, 'entities.facilities.entities');

  const stationsWithProjects = _.map(visibleStations, station => {
    const lat = _.get(station, 'location.position._lat');
    const lng = _.get(station, 'location.position._long');
    const projectsNearStation = getProjectWithinRadius(filteredProjects, { lat, lng }, 0, radius);
    const facilityScores = calculateFacilityScore(allFacilities, lat, lng);

    return {
      ...station,
      projects: projectsNearStation,
      stats: {
        ...getProjectsStats(projectsNearStation),
        facilityScore: _.sumBy(facilityScores, score => score.value) / _.size(facilityScores),
      },
    }
  });
  return {
    stations: stationsWithProjects,
    radius,
    viewOptionsLines: analysisReducer.viewOptionsLines,
  };
}
export default connect(mapStateToProps)(class extends React.Component {

  static getDerivedStateFromProps (nextProps, prevState) {
    if (nextProps.viewOptionsLines !== prevState.viewOptionsLines) {
      return {
        viewOptionsLines: nextProps.viewOptionsLines,
      }
    }
    return null;
  }

  state = {
    view: 'salePrice',
    openPopover: false,
    viewOptionsLines: this.props.viewOptionsLines,
  }

  handleChangeRadius = e => {
    const { dispatch } = this.props;
    dispatch({ type: 'DOMAIN/MAP/ANALYSIS/RADIUS/CHANGE', value: e.target.value });
  }

  handleChangeView = e => {
    this.setState({
      view: e.target.value,
    });
  }

  handleToggle = value => () => {
    const { dispatch } = this.props;
    const { viewOptionsLines } = this.state;
    const currentIndex = viewOptionsLines.indexOf(value);
    const newChecked = [...viewOptionsLines];

    if (currentIndex === -1) {
      newChecked.push(value);
    } else {
      newChecked.splice(currentIndex, 1);
    }

    this.setState({
      viewOptionsLines: newChecked,
    });
    
    dispatch({
      type: 'DOMAIN/MAP/ANALYSIS/VIEWOPTION/LINES/CHANGE',
      values: newChecked,
    });
  };

  openPopover = (e) => {
    this.setState({
      openPopover: true,
    })
  }

  handleClosePopover = (e) => {
    this.setState({
      openPopover: false,
    })
  }

  render() {
    const { stations, radius } = this.props;
    const { view, viewOptionsLines } = this.state;

    const legendColor = getMinMaxColorByScoreType(view);
    console.log('legendColor', legendColor);

    const lines = [
      { value: 'bts.sukhumvit', label: 'BTS เขียวอ่อน' },
      { value: 'bts.silom', label: 'BTS เขียวเข้ม' },
      { value: 'mrt', label: 'MRT' },
    ];

    const minValue = _.chain(stations)
      .map(station => _.get(station, `stats.${view}`))
      .min()
      .value();

    const maxValue = _.chain(stations)
      .map(station => _.get(station, `stats.${view}`))
      .max()
      .value();

    return (
      <React.Fragment>
        {_.map(stations, station => {
          const lat = _.get(station, 'location.position._lat');
          const lng = _.get(station, 'location.position._long');
          const value = _.get(station, `stats.${view}`);
          const color = getColorByScoreType(value, minValue, maxValue, view);
          return <StationCircle id={station.id} color={color} lat={lat} lng={lng} stats={station.stats} view={view} />
        })}
        <OverlayToolbarContainer>
          <ToolbarInner>
            <FormControl variant="outlined">
              <Button
                buttonRef={c => this.anchorEl = c}
                onClick={this.openPopover}
                color="default"
                variant="contained"
              >View Option</Button>
              <Popover
                open={this.state.openPopover}
                anchorEl={this.anchorEl}
                onClose={this.handleClosePopover}
                anchorOrigin={{
                  vertical: 'bottom',
                  horizontal: 'left',
                }}
                transformOrigin={{
                  vertical: 'top',
                  horizontal: 'left',
                }}
              >
                <List component="div" disablePadding dense style={{ background: '#f7f7f7' }}>
                  {lines.map(item => (
                    <ListItem
                      button
                      key={item.value}
                      style={{ paddingTop: 8, paddingBottom: 8 }}
                      onClick={this.handleToggle(item.value)}
                    >
                      <Checkbox
                        checked={viewOptionsLines.indexOf(item.value) !== -1}
                        tabIndex={-1}
                        disableRipple
                      />
                      <ListItemText
                        primary={item.label}
                        style={{ fontSize: 12 }}
                      />
                    </ListItem>
                  ))}
                </List>
              </Popover>
            </FormControl>
            <FormControl variant="default">
              <Select value={radius} onChange={this.handleChangeRadius}>
                <MenuItem value={200}>200m</MenuItem>
                <MenuItem value={400}>400m</MenuItem>
                <MenuItem value={600}>600m</MenuItem>
                <MenuItem value={1000}>1km</MenuItem>
              </Select>
            </FormControl>
            <FormControl variant="default">
              <Select value={this.state.view} onChange={this.handleChangeView}>
                <MenuItem value="salePrice">Sale Price</MenuItem>
                <MenuItem value="capitalGain">Captial Gain</MenuItem>
                <MenuItem value="startingPrice">Starting Price</MenuItem>
                <MenuItem value="rentPrice">Rental Price</MenuItem>
                <MenuItem value="rentalYield">Rental Yield</MenuItem>
                <MenuItem value="facilityScore">Facility Score</MenuItem>
              </Select>
            </FormControl>
          </ToolbarInner>
          <Button
            variant="raised"
            color="primary"
            className="noprint"
            onClick={() => window.print()}
          >
            <IconLaunch style={{ fontSize: '1em', marginRight: 5, position: 'relative', top: -2 }} /> Export result
          </Button>
        </OverlayToolbarContainer>
        <LegendWrapper>
          Low <LegendBar min={legendColor.min} max={legendColor.max} /> High
        </LegendWrapper>
      </React.Fragment>
    );
  }
});
