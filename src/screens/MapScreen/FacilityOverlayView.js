import React from 'react';
import _ from 'lodash';
import styled from 'styled-components';
import { connect } from 'react-redux';
// import * as Colors from '../../components/Color';
import { OverlayView, Marker, Circle, GroundOverlay } from '../../components/Map';

import { getDistance, offsetPosition } from '../../utils/map-utils';

// import walkRing from './img/walk-ring.png';
// import driveRing from './img/drive-ring.png';

const FacilityTooltip = styled.div`
  background: rgba(0, 0, 0, 0.7);
  padding: 4px 8px;
  color: white;
  display: ${props => props.visible ? 'block' : 'none'};
`;

const getPixelPositionOffset = (width, height) => ({
  x: -(width / 2),
  y: -(height + 40)
});

// const getCircleLabelPixelPositionOffset = (width, height) => ({
//   x: -(width / 2),
//   y: -(height / 2),
// });


const mapStateToProps = (state, ownProps) => {
  const { radius } = ownProps;

  // Domain
  const mapReducer = _.get(state, 'domain.map');
  const { facilityViewOptionList } = mapReducer;

  // const facilities = _.get(state, 'facilities.data');
  const facilities = _.get(state, 'entities.facilities.entities');

  // Find current lat, long position
  const currentLat = ownProps.pointOfInterest.location.position._lat;
  const currentLong = ownProps.pointOfInterest.location.position._long;

  // Find distance between facilities and current position
  const facilitiesWithDistance = _.map(facilities, facility => {
    return { ...facility, distance: getDistance(currentLat, currentLong, _.toNumber(facility.location.lat), _.toNumber(facility.location.lon)) };
  });
  const nearbyFacilities = _.filter(facilitiesWithDistance, facility => facility.distance < 0.001 * radius); // 1km
  const visibleFacilities = _.filter(nearbyFacilities, facility => _.includes(facilityViewOptionList, facility.category));
  return {
    facilityViewOptionList,
    facilities: visibleFacilities,
  };
};

export default connect(mapStateToProps)(
  class extends React.PureComponent {
    state = {
      hoverMark: null,
    };

    handlePopoverOpen = id => {
      this.setState({ hoverMark: id });
    }
    handlePopoverClose = id => {
      const currHover = this.state.hoverMark;
      if (!_.isEqual(currHover, id)) {
        this.setState({ hoverMark: null });
      }
    };

    renderLocationRadius = (lat, lng, radius) => {

      // Walk 10min, (1000m)
      // const walkingRadiusFactor = 0.008;
      // const drivingRadiusFactor = 0.03;
      // const labelPos = offsetPosition(lat, lng, walkingRadius * Math.cos(0.2), -radius * Math.sin(0.2));
  
      return (
        <React.Fragment>
          {/* <GroundOverlay
            defaultUrl={walkRing}
            defaultBounds={{
              east: lng + walkingRadiusFactor, // -74.12544,
              west: lng - walkingRadiusFactor, // -74.22655,
              north: lat + walkingRadiusFactor, // 40.773941,
              south: lat - walkingRadiusFactor, // 40.712216,
            }}
            defaultOpacity={1}
          /> */}
          {/* <GroundOverlay
            defaultUrl={driveRing}
            defaultBounds={{
              east: lng + drivingRadiusFactor, // -74.12544,
              west: lng - drivingRadiusFactor, // -74.22655,
              north: lat + drivingRadiusFactor, // 40.773941,
              south: lat - drivingRadiusFactor, // 40.712216,
            }}
            defaultOpacity={1}
          /> */}
          {/* <OverlayView
            position={labelPos}
            mapPaneName={OverlayView.OVERLAY_MOUSE_TARGET}
            getPixelPositionOffset={getCircleLabelPixelPositionOffset}
          >
            <div
              style={{
                background: '#1C8700',
                color: 'white',
                padding: '6px 12px',
                textAlign: 'center',
                lineHeight: 0.8,
              }}
            >10min walk</div>
          </OverlayView> */}
        </React.Fragment>
      )
    }

    render() {
      const { facilities, pointOfInterest, getIconUrl } = this.props;
      const { hoverMark } = this.state;
      // console.log('pointOfInterest', pointOfInterest);
      const lat = _.get(pointOfInterest, 'location.position._lat');
      const lng = _.get(pointOfInterest, 'location.position._long');

      return (
        <React.Fragment>
          {this.renderLocationRadius(lat, lng)}
          {_.map(facilities, facility => {
          const isHover = _.isEqual(hoverMark, facility.id);
          return (
            <OverlayView
              key={facility.id}
              position={{
                lat: _.get(facility, 'location.lat'),
                lng: _.get(facility, 'location.lon')
              }}
              mapPaneName={OverlayView.OVERLAY_MOUSE_TARGET}
              getPixelPositionOffset={getPixelPositionOffset}
              onMouseOut={() => this.handlePopoverClose(facility.id)}
            >
              <div>
                <Marker
                  key={facility.id}
                  position={{
                    lat: _.toNumber(_.get(facility, 'location.lat')),
                    lng: _.toNumber(_.get(facility, 'location.lon'))
                  }}
                  options={{
                    icon: {
                      url: getIconUrl(facility.category),
                      size: { width: 36, height: 37 },
                      scaledSize: { width: 36, height: 37 },
                    }
                  }}
                  onMouseOver={() => this.handlePopoverOpen(facility.id)}
                  onMouseOut={() => this.handlePopoverClose(facility.id)}
                />
                <FacilityTooltip
                  visible={isHover}
                  key={`${facility.id}-tooltip`}
                >{facility.title.en}
                </FacilityTooltip>
              </div>  
            </OverlayView>
          );

        })}
        </React.Fragment>
      );
    }
  }
);

/*
<OverlayView
            key={facility.id}
            position={{
              lat: _.get(facility, 'location.lat'),
              lng: _.get(facility, 'location.lon')
            }}
            mapPaneName={OverlayView.OVERLAY_MOUSE_TARGET}
            getPixelPositionOffset={getPixelPositionOffset}
          >

          </OverlayView>
*/