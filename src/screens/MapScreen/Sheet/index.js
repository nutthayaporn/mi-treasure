import React from 'react';
import _ from 'lodash';
import ReactDataSheet from 'react-datasheet';

import 'react-datasheet/lib/react-datasheet.css';

export default class extends React.Component {

  constructor (props) {
    super(props)
    this.state = {
      grid: [
        [{value:  1}, {value:  3}],
        [{value:  2}, {value:  4}]
      ]
    }
  }

  componentDidMount() {
    const { db } = this.props;
    const projectsRef = db.collection('meerkat_projects');

    projectsRef
      .get()
      .then((querySnapshot) => {
       
        const grid = _.map(querySnapshot.docs, doc => {
          const data = doc.data();
          return [
            { value: doc.id },
            { value: data.title },
            { value: 0 },
            { value: 0 },
          ]
        });

        this.setState({
          grid,
        });
      });
  }

  render() {
    return (
      <div>
        <ReactDataSheet
          data={this.state.grid}
          valueRenderer={(cell) => cell.value}
          onCellsChanged={changes => {
            const grid = this.state.grid.map(row => [...row])
            changes.forEach(({cell, row, col, value}) => {
              grid[row][col] = {...grid[row][col], value}
            })
            this.setState({grid})
          }}
        />
      </div>
    );
  }  
}
