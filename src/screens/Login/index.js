import React from 'react';
import { withRouter } from 'react-router-dom';

import Button from '@material-ui/core/Button';
import TextField from '../../components/TextField';

import {
  BackgroundContainer,
  Wrapper,
  FormContainer,
  Form,
  ErrorMessage,
} from './styles';

const INITIAL_STATE = {
  email: '',
  password: '',
  error: '',
  errorCode: null,
};

class LoginScreen extends React.Component {

  state = { ...INITIAL_STATE };

  afterSignInSuccess = (user) => {
    const { history } = this.props;
    console.log('afterSignInSuccess', user);
    setTimeout(() => {
      history.push('/map');
    }, 200);
  }

  handleError = (error) => {
    const errorMessage = error.message || 'Cannot login';
    this.setState({ error: errorMessage });
    // notify.show(`(${errorCode}) ${errorMessage}`, 'error', 10000);
  }

  handleChangeText = (name, value) => {
    this.setState({
      [name]: value,
      error: '',
    });
  }

  signInUser = async (e) => {
    if (e) e.preventDefault();
    this.setState({ error: '' });

    const { auth } = this.props;
    const { email, password } = this.state;
    try {
      const user = await auth.signInWithEmailAndPassword(email, password);
      this.afterSignInSuccess(user);

    } catch (error) {
      console.log('signInError', error);
      this.handleError(error);
    }
  }

  signInWithGoogle = async (e) => {
    e.preventDefault();
    const { auth } = this.props;
    try {
      const user = await auth.signInWithGoogle();
      this.afterSignInSuccess(user);
      // setTimeout(() => this.afterSignInSuccess(user), 250);
      // Delay to fix bug, userInfo not visible

    } catch (error) {
      console.log('signInError', error);
      this.handleError(error);
    }
  }

  signInWithFacebook = async (e) => {
    e.preventDefault();
    const { auth } = this.props;
    try {
      const user = await auth.signInWithFacebook();
      this.afterSignInSuccess(user);
    } catch (error) {
      console.log('signInError', error);
      this.handleError(error);
    }
  }

  handleKeyUp = (e) => {
    if (e.keyCode === 13) {
      this.signInUser();
    }
  }

  render() {
    const { email, password, error } = this.state;
    return (
      <BackgroundContainer>
        <Wrapper>
          <FormContainer>
            <Form onSubmit={this.signInUser}>
              <img src="/img/theagent-logo.svg" alt="The Agent Logo" />
              { error !== '' && <ErrorMessage>{error}</ErrorMessage>}
              <TextField
                id="email"
                label="Email"
                value={email}
                onChange={e => this.handleChangeText('email', e.target.value)}
                margin="normal"
                fullWidth
              />
              <TextField
                type="password"
                id="password"
                label="Password"
                margin="normal"
                value={password}
                onKeyUp={this.handleKeyUp}
                onChange={e => this.handleChangeText('password', e.target.value)}
                fullWidth
              />
              <Button variant="raised" color="primary" style={{ margin: '10px 0' }} onClick={this.signInUser}>Sign in</Button>
              <hr />
              <Button type="button" style={{ display: 'block', width: '100%', background: '#ff3b3b', border: 'none', color: 'white', marginBottom: 20 }} onClick={this.signInWithGoogle}>Sign in with Google</Button>
              
              {/* engine.type === 'firebase' &&
                <React.Fragment>
                  <hr />
                  <div>
                    <Button type="button" style={{ display: 'block', width: '100%', background: '#ff3b3b', border: 'none', color: 'white', marginBottom: 20 }} onClick={this.signInWithGoogle}>Sign in with Google</Button>
                    <Button type="button" style={{ display: 'block', width: '100%', background: '#1b6cda', border: 'none', color: 'white' }} onClick={this.signInWithFacebook}>Sign in with Facebook</Button>
                  </div>
                </React.Fragment>
              */}
            </Form>
          </FormContainer>
        </Wrapper>
      </BackgroundContainer>
    );
  }
}

export default withRouter(LoginScreen);
