import React from 'react';
import styled from 'styled-components';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import NewsCard from '../../components/NewsCard';

const Container = styled.div`
  background: #fafafa;
  width: 100vw;
  height: 100vh;
  display: flex;
  align-items: center;
  justify-content: center;

`;

export default class extends React.Component {
  render() {
    return (
      <Container>
        <NewsCard onClick={() => console.log('onClick')}/>
      </Container>
    );
  }
}