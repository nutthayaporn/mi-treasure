import React from 'react';
import styled from 'styled-components';
import _ from 'lodash';
import { connect } from 'react-redux';

import Card from '@material-ui/core/Card';
import Button from '@material-ui/core/Button';


import CompareTable from '../../components/CompareTable';
import WatermarkOverlay from '../../components/WatermarkOverlay';
import { H3 } from '../../components/Typography';
import * as Color from '../../components/Color';

import { getProjectStat } from '../../utils/stat-utils';
import { getDistance } from '../../utils/map-utils';


const Container = styled.div`
  padding-top: 56px;
  padding-bottom: 38px;
  background: #fafafa;
  width: 100%;
  height: calc(100vh - 155px);
  display: flex;
  flex-direction: column;
  align-items: center;
  overflow: auto;
`;

const Content = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  width: 89%;
  -webkit-print-color-adjust: exact;
`;

const TitleHolder = styled.div`
  text-align: center;
  margin-bottom: 40px;
  display: flex;
  flex-direction: row
  justify-content: space-between;
  flex-wrap: wrap;
  width: 100%;
  display: block;
`;

const TitleText = styled(H3)`
  color: ${Color.primary} !important;
  font-size: 1.25rem !important;
`;
const StyledCard = styled(Card)`
  margin-bottom: 30px;
`;

const CompareHolder = styled.div`
  width: 100%;
`;


const Title = ({ selectedList }) => {
 const text = _.map(selectedList, 'title').join('/');
  return (
    <TitleHolder>
      <Button
        disabled
        variant="raised"
        style={{ backgroundColor: '#fafafa' }}
        className="noprint"
      />
      <TitleText>Compare Average by {text}</TitleText>
        <Button
        variant="raised"
        color="primary"
        className="noprint"
        onClick={() => window.print()}
      >
        Export screen
      </Button>
    </TitleHolder>
  );
}

const mapStateToProps = (state, ownProps) => {
  const paramsIds = _.get(ownProps, 'match.params.ids');
  const poi = _.get(ownProps, 'match.params.poi');
  const entitiesProject = _.get(state, 'entities.projects.entities');
  const entitiesPoint = _.get(state, 'entities.pointOfInterests.entities');
  const ids = _.split(paramsIds, ',');
  const compareData = _.pick(entitiesProject, ids);
  const pointInterest = _.pick(entitiesPoint, poi);
  // console.log('pointInterest', entitiesPoint, pointInterest, poi);
  return {
    compareData,
    pointInterest,
    poi,
  };
}

export default connect(mapStateToProps)(class extends React.Component {
  getLocation = (data, poi) => {
    const { pointInterest } = this.props;
    const lat1 = _.get(data, 'location.position._lat', 0);
    const long1 = _.get(data, 'location.position._long', 0);
  
    let lat2 = '';
    let long2 = '';
    let locatFrom = ';'
  
    if (/^@/.test(poi)) {
      // Is Lat Lng
      let [lat, lng] = _.split(_.replace(poi, '@', ''), ',');
      lat2 = _.toNumber(lat);
      long2 = _.toNumber(lng);
      locatFrom = _.replace(poi, '@', '');
    } else {
      // is poiId
      lat2 = _.get(pointInterest, [poi, 'location', 'position', '_lat'], 0);
      long2 = _.get(pointInterest, [poi, 'location', 'position', '_long'], 0);
      locatFrom = _.get(pointInterest, [poi, 'title', 'en']);
    }
    return {
      distance: getDistance(lat1, long1, lat2, long2),
      locatFrom,
    }
    
  }
  render() {
    // console.log('PROPS compare', this.props.compareData);
    const { compareData, poi } = this.props;

    const selectedList = _.map(compareData, (data, key) => {
      const sat = getProjectStat(data);
      const location = this.getLocation(data, poi);
  
      const result = {
        id: key,
        title: _.get(data, 'title.en'),
        location: `${Math.round(location.distance * 1000)}m from ${location.locatFrom}` ,
        yearOpen: _.get(data, 'yearOpen'),
        yearComplete: _.get(sat, 'yearComplete'),
        avgPpsqm: _.get(sat, 'salePrice'),
        avgRpsqm: _.get(sat, 'rentPrice'),
        ...sat
      }
      return result;
    })

    return (
      <Container>
        <Content>
        <Title selectedList={selectedList}/>
        <CompareHolder>
          <CompareTable selectedList={selectedList}/>
        </CompareHolder>
        </Content>
        <WatermarkOverlay />
      </Container>
    );
  }
});
