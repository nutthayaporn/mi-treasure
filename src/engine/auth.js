import firebase from './firebase';
import axios from 'axios';
import _ from 'lodash';

const USER_COLLECTIONS = 'users';

export const signInWithEmailAndPassword = (email, password) =>
  firebase.auth().signInWithEmailAndPassword(email, password)
    .then(async result => {
      const { user } = result;
      return user.toJSON();
    })

export const signInWithGoogle = () => {
  const provider = new firebase.auth.GoogleAuthProvider();
  return firebase.auth().signInWithPopup(provider)
    .then(async result => {
      const user = result.user;
      return user.toJSON();
    });
}

export const signInWithFacebook = () => {
  const provider = new firebase.auth.FacebookAuthProvider();
  return firebase.auth().signInWithPopup(provider)
    .then(async result => {
      const user = result.user;
      // const uid = result.user.uid;
      return user.toJSON();
    });
}

export const signOut = () => {
  return firebase.auth().signOut();
}

export const onAuthStateChanged = (cb) => firebase.auth().onAuthStateChanged(async user => {
  if (user === null) cb(user);
  else {
    const userWithInfo = await firebase.auth().currentUser.getIdTokenResult()
    .then((idTokenResult) => ({ ...user.toJSON(), roles: idTokenResult.claims.roles }));
    cb(userWithInfo);
  }
});

export default {
  signInWithEmailAndPassword,
  signInWithGoogle,
  signInWithFacebook,
  signOut,
  onAuthStateChanged,
}