import _ from 'lodash';
import { db } from '../engine';

const PROJECT_COLLECTION = 'projects';

export const fetchPointOfInterest = (poiId) => {
  return db.collection('locations').doc(poiId).get()
    .then(doc => {
      return {
        id: doc.id,
        ...doc.data(),
      };
    });
}

export const fetchFacilities = () => {
  return db.collection('facilities').get()
    .then(querySnapshot => {
      return _.map(querySnapshot.docs, doc => doc.data());
    })
}

export const fetchPointOfInterests = (lat, lng, radius) => {
  const publicTransportPromise = db.collection('locations').get()
    .then(querySnapshot => {
      return _.map(querySnapshot.docs, doc => ({
        id: doc.id,
        ...doc.data(),
      }));
    }).catch(err => {
      console.log('err',  err)
    });
  
  const projectPromise = db.collection(PROJECT_COLLECTION).get()
    .then(querySnapshot => {
      return _.map(querySnapshot.docs, doc => {
        const data = doc.data();
        return {
          id: doc.id,
          ...data,
          type: 'PROJECT',
        }
      });
    });
  
  return Promise.all([publicTransportPromise, projectPromise])
    .then(([publicTransports, projects]) => {
      return [...publicTransports, ...projects];
    });
} 

export const fetchNearbyPlaces = (lat, lng, radius) => {
  return fetch(`${process.env.REACT_APP_API}/api/place/nearbysearch?key=${process.env.REACT_APP_GOOGLE_APIKEY_SERVER}&location=${lat},${lng}&radius=500`)
    .then(res => res.json())
    .then(res => {
      return res.data;
    });
}

export const fetchProjects = (lat, lng, radius) => {
  
}