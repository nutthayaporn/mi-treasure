import { createMuiTheme } from '@material-ui/core/styles';

const Color = {
  primary: '#EB6323',
}

const theme = createMuiTheme({
  // Colors
  palette: {
    type: 'light',
    primary: {
      main: Color.primary,
    },
    secondary: {
      main: '#ff3b3b',
    }
  },

  // Typography
  typography: {
    fontFamily: ['SukhumvitSet-Medium'].join(','),
    display4: {
      fontSize: '1.42rem',
      fontFamily: 'SukhumvitSet-Bold',
      fontWeight: 'bold',
      color: '#303030',
    },
    display3: {
      fontSize: '1.142rem',
      fontFamily: 'SukhumvitSet-Bold',
      fontWeight: 'bold',
      color: '#303030',
    },
    display2: {
      fontSize: '1rem',
      fontFamily: 'SukhumvitSet-Bold',
      fontWeight: 'bold',
      color: '#303030',
    },
    display1: {
      fontSize: '1rem',
      fontWeight: 500,
      color: '#666666',
    },
    headline: {
      fontSize: '0.857rem',
      fontFamily: 'SukhumvitSet-Bold',
      fontWeight: 'bold',
      color: '#666666',
    },
    title: {
      fontSize: '1rem',
      fontWeight: 500,
      color: '#666666',
    }
  },

  // Override base styles
  overrides: {
    MuiCheckbox: {
      root: {
        width: 14,
        height: 14,
      },
    },
    MuiButton: {
      root: {
        minHeight: 32,
        padding: '7px 16px 6px',
      }
    },
    MuiInput: {
      root: {
        boxShadow: '0 4px 16px 0 rgba(48, 48, 48, 0.05)'
      },
      input: {
        color: Color.primary,
        padding: '6px 16px',
        background: 'white',
        borderRadius: 4,  
      }
    },
    MuiFormLabel: {
      root: {
        fontSize: 18,
        fontWeight: 500,
      }
    },
    MuiTabs: {
      indicator: {
        bottom: 0,
        top: 'auto',
        height: 4
      }
    },
    MuiTab: { 
      selected: {
        background: "rgba(239, 99 ,35, 0.1)"
        // color: 'white !important',
      },
      // wrapper: {
      //   flexDirection: 'row',
      // }
    },
    MuiSvgIcon: {
      root: {
        fontSize: 20
      }
    }
  },

  // Props
  props: {
    MuiButtonBase: {
      // The properties to apply
      disableRipple: true, // No more ripple, on the whole application 💣!
    },
    MuiCheckbox: {
      color: 'primary',
    },
    MuiInputLabel: {
      disableAnimation: true,
    }
  }  
});


export const adminTheme = createMuiTheme({
  // Colors
  palette: {
    type: 'light',
    primary: {
      main: Color.primary,
    },
    secondary: {
      main: '#ff3b3b',
    }
  },

  // Typography
  typography: {
    fontFamily: ['SukhumvitSet-Medium'].join(','),
  },
});


export default theme;