import React from 'react';
import _ from 'lodash';

import { LinearProgress } from 'material-ui/Progress'; 

export default (fetchData) => WrappedComponent => {
  return class extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        loading: false,
        success: true,
        data: [],
        error: undefined,
      }
    }

    componentDidMount() {
      this.load(this.props);
    }

    componentWillReceiveProps (nextProps) {
      if (!_.isEqual(this.props, nextProps)) {
        this.load(nextProps);
      }
    }

    reload = () => {
      this.load(this.props);
    }

    load = async (props) => {
      this.setState({ loading: true });
      // console.log('typeof fetchData', typeof fetchData);
      try {
      const data = await fetchData(props)
        this.setState({
          data: data,
          error: undefined,
          loading: false,
        });
      } catch (error) {
        this.setState({
          error,
          data: undefined,
          loading: false,
        });
      }

    }

    render() {
      if (this.state.loading) return <LinearProgress />;
      return (
        <WrappedComponent
          {...this.props}
          {...this.state}
          reload={this.reload}
        />
      );
    }
  }
}