const initialState = {
  viewOptionsLines: ['bts.sukhumvit', 'bts.silom', 'mrt'],
};

export default (state = initialState, action) => {
  switch (action.type) {
    case 'DOMAIN/MAP/ANALYSIS/VIEWOPTION/LINES/CHANGE':
      return {
        ...state,
        viewOptionsLines: action.values,
      }
    default: return state;
  }
}