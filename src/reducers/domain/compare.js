import _ from 'lodash';

const initialState = {
  ids: [],
}

export default (state = initialState, action) => {
  switch (action.type) {
    case 'COMPARE/ADD/ALL':
      return {
        ...state,
        ids: _.uniq([...state.ids, ...action.ids]),
      }
    case 'COMPARE/ADD': {
      return {
        ...state,
        ids: [...state.ids, action.id],
      }
    }
    case 'COMPARE/REMOVE/ALL':
      return {
        ...state,
        ids: [],
      }
    case 'COMPARE/REMOVE': {
      return {
        ...state,
        ids: _.reject(state.ids, id => id === action.id),
      }
    }
    default: return state;
  }
}