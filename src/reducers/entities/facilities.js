import _ from 'lodash';

const initialState = {
  // data: {},
  entities: {},
}

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case 'ENTITIES/FACILITIES/RECEIVED':
      return {
        ...state,
        entities: _.reduce(action.items, (sum, item) => {
          return { ...sum, [item.id]: item }
        }, state.data),
      }
    default: return state;
  }
}

export default reducer;