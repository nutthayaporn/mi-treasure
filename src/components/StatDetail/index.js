import React from 'react';
import _ from 'lodash';
import styled from 'styled-components';
import numeral from 'numeral';
import * as Color from '../Color';
import { P } from '../Typography';

const MetaItem = styled.div`
  flex: 1 0 25%;
  padding: 6px 0 0;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;


const MainInfo = styled.div`
  text-align: right;
  border-bottom: 1px solid #d8d8d8;
  padding-bottom: 4px;
`;

const MetaInfo = styled.div`
  display: flex;
`;

const PriceRow = styled(P)`
  color: #787878;
  margin-bottom: 0;
`;

const Text = styled.span`
  font-size: 11px;
  color: rgba(0, 0, 0, 0.54);
  text-align: center;
  ${props => props.value && `font-weight: bold;`}
`;

const EmText = styled.span`
  font-size: 20px;
  font-weight: bold;
  display: inline-block;
  margin-left: 20px;
`;
const MutedText = styled.span`
  font-size: 14px;
  color: #9a9a9a;
`;

export default class extends React.PureComponent {
  
  render() {
    const { yearComplete, salePrice = 0, capitalGain = 0, startingPrice = 0, rentPrice = 0, rentalYield = 0 } = this.props;
    return (
      <div>
        <MainInfo>
          <PriceRow>Sale <EmText style={{ color: Color.primary }}>
            {_.isNaN(salePrice) ? 'N/A' : numeral(salePrice).format('0,0a')}</EmText> <MutedText>/sq.m.</MutedText>
          </PriceRow>
          <PriceRow>Rent <EmText style={{ color: '#c29b60', fontSize: 14 }}>
            {_.isNaN(rentPrice) ? 'N/A' : numeral(rentPrice).format('0,0.00')}</EmText> <MutedText>/sq.m.</MutedText>
          </PriceRow>
        </MainInfo>
        <MetaInfo>
          <MetaItem>
            <Text>Year</Text>
            <Text value>{yearComplete}</Text>
          </MetaItem>
          <MetaItem>
            <Text>Capital Gain</Text>
            <Text value>{_.isNaN(capitalGain) ? 'N/A' : numeral(capitalGain).format('0.00%')}</Text>
          </MetaItem>
          <MetaItem>
            <Text>Starting Price</Text>
            <Text value>{_.isNaN(startingPrice) ? 'N/A' : numeral(startingPrice).format('0,0a')}</Text>
          </MetaItem>
          <MetaItem>
            <Text>Rental Yield</Text>
            <Text value>{_.isNaN(rentalYield) ? 'N/A' : numeral(rentalYield).format('0.00%')}</Text>
          </MetaItem>
        </MetaInfo>
      </div>
    );
  }
}
