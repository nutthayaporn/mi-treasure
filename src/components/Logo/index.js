import React from 'react';
import styled from 'styled-components';
import * as Color from '../Color';

const Logo = styled.div`
  box-sizing: border-box;
  height: 60px;
  padding: 9px 18px;
  position: relative;
  display: flex;
  justify-content: center;
  align-items: center;

  &::before {
    content: '';
    width: 172px;
    height: 60px;
    position: absolute;
    left: 0;
    top: 0;
    bottom: 0;
    height: 100%;
    background: ${Color.primary};
    z-index: 1;
  }

  img {
    display: block;
    max-width: 100%;
    position: relative;
    z-index: 2;
  }
`;


export default () => (
  <Logo>
    <img src="/img/theagent-logo.svg" alt="The Agent" width={300} />
  </Logo>
)