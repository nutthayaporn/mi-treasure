import React from "react";
import PT from "prop-types";
import styled from "styled-components";
import * as Color from "../Color";
import { H6 } from "../Typography";

const TagHolder = styled(H6)`
  padding: 9px 15px;
  border-radius: 15px;
  background-color: ${props => props.color};
  text-transform: capitalize;

  font-size: 12px !important;
  font-weight: 600 !important;
  text-align: center;
  color: #ffffff !important;
`;

class Tag extends React.Component {
  static propTypes = {
    text: PT.string
  };

  static defaultProps = {
    text: 'Tag'
  };
  getColor = (type) => {
    switch (type){
      case 'articles': return Color.Tag_Article;
      case 'reviews': return Color.Tag_Review;
      default:
      return Color.primary;
    }

  }
  render() {
    const { text } = this.props;
    return (
      <TagHolder color={this.getColor(text)}>{text}</TagHolder>
    );
  }
}

export default Tag;
