import styled from 'styled-components';
import Typography from '@material-ui/core/Typography';

export const H1 = styled(Typography).attrs({ variant: 'display4', gutterBottom: true })``;
export const H2 = styled(Typography).attrs({ variant: 'display3', gutterBottom: true })``;
export const H3 = styled(Typography).attrs({ variant: 'display2', gutterBottom: true })``;
export const H4 = styled(Typography).attrs({ variant: 'display1', gutterBottom: true })``;
export const H5 = styled(Typography).attrs({ variant: 'headline', gutterBottom: true })``;
export const H6 = styled(Typography).attrs({ variant: 'title', gutterBottom: true })``;
export const P = styled(Typography).attrs({ variant: 'body1', gutterBottom: true })``;