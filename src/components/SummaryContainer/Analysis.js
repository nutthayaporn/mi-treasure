import React from 'react';
import _ from 'lodash';
import Fuse from 'fuse.js';
import numeral from 'numeral'

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import Grid from '@material-ui/core/Grid';
import Avatar from '@material-ui/core/Avatar';
import Tooltip from '@material-ui/core/Tooltip';
import Table from '@material-ui/core/Table';
import TableHead from '@material-ui/core/TableHead';
import TableBody from '@material-ui/core/TableBody';
import TableRow from '@material-ui/core/TableRow';
import TableSortLabel from '@material-ui/core/TableSortLabel';
import TableCell from '@material-ui/core/TableCell';
import TextField from '@material-ui/core/TextField';

import { filterProjectsByQuery } from '../../screens/MapScreen/utils';
import { getDistance } from '../../utils/map-utils';
import { getProjectStat } from '../../utils/stat-utils';
import { getColorByScoreType } from '../../utils/color-utils';
import { calculateFacilityScore } from '../../utils/facilities-utils';
import { withStyles } from '@material-ui/core/styles';

const fuseOptions = {
  shouldSort: true,
  threshold: 0.2,
  location: 0,
  distance: 100,
  maxPatternLength: 32,
  minMatchCharLength: 1,
  keys: [
    "title.en",
    "title.th"
  ]
};

const DenseTableCell = withStyles({
  root: {
    paddingLeft: 6,
    paddingRight: 6,
  }
})(TableCell);

const getProjectWithinRadius = (projects, { lat, lng }, inner, outer) => {
  return _.filter(projects, p => {
    // console.log('p', p);
    const projectLat = _.get(p, 'location.position._lat');
    const projectLng = _.get(p, 'location.position._long');
    const d = getDistance(projectLat, projectLng, lat, lng);
    // console.log(projectLat, projectLng, lat, lng, d);
    return d <= 0.001 * outer && d >= 0.001 * inner;
  });
};

const getProjectsStats = (projects) => {

  if (_.size(projects) === 0) {
    return { salePrice: 0, capitalGain: 0, startingPrice: 0, rentPrice: 0, rentalYield: 0 };
  }
  
  const allProjectStats = _.map(projects, getProjectStat);
 
  const capitalGainValues = _.compact(_.map(allProjectStats, stat => stat.capitalGain));
  const rentPriceValues = _.compact(_.map(allProjectStats, stat => stat.rentPrice));
  const rentalYieldValues = _.compact(_.map(allProjectStats, stat => stat.rentalYield));
  const salePriceValues = _.compact(_.map(allProjectStats, stat => stat.salePrice));
  const startingPriceValues = _.compact(_.map(allProjectStats, stat => stat.startingPrice));

  return {
    salePrice: _.sum(salePriceValues) / _.size(salePriceValues),
    capitalGain: _.sum(capitalGainValues) / _.size(capitalGainValues),
    startingPrice: _.sum(startingPriceValues) / _.size(startingPriceValues),
    rentPrice: _.sum(rentPriceValues) / _.size(rentPriceValues),
    rentalYield: _.sum(rentalYieldValues) / _.size(rentalYieldValues)
  };
}

const getStationColor = stationId => {
  if (_.includes(stationId, 'BTS.E') || _.includes(stationId, 'BTS.N')) return '#77cc00';
  if (_.includes(stationId, 'BTS.W') || _.includes(stationId, 'BTS.S')) return '#246b5b';
  if (_.includes(stationId, 'MRT.')) return '#1964b7';
  return '#000';
}

const StyledTableRow = withStyles({
  root: {
    cursor: 'pointer',
    '&$selected': {
      backgroundColor: '#f00',
    }
  }
})(TableRow);


const StationTableRow = connect(
  (state, ownProps) => ({
    hilight: _.includes(_.get(state, 'domain.map.analysisHilightStations'), ownProps.id), 
  }),
  dispatch => bindActionCreators({
    toggleSelectStation: stationId => ({ type: 'DOMAIN/MAP/ANALYSIS/HILIGHT_STATION/TOGGLE', stationId }),
    selectStation: stationId => ({ type: 'DOMAIN/MAP/ANALYSIS/HILIGHT_STATION/SELECT', stationId }),
    unselectStation: stationId => ({ type: 'DOMAIN/MAP/ANALYSIS/HILIGHT_STATION/UNSELECT', stationId }),
  }, dispatch),
)(class extends React.PureComponent {
  handleClick = e => {
    const { id, toggleSelectStation } = this.props;
    toggleSelectStation(id);
  }
  render() {
    const { id, title, stats, keys, minmax, hilight } = this.props; 
    return (
      <StyledTableRow
        key={id}
        hover
        selected={hilight}
        onClick={this.handleClick}
        // onMouseOver={e => onHover(id)}
      >
        <TableCell>
          <Avatar
            style={{
              fontFamily: 'Arial',
              fontSize: '0.9em',
              fontWeight: 'bold',
              width: 30,
              height: 30,
              backgroundColor: getStationColor(id),
            }}
          >
            {_.replace(id, /(BTS.|MRT.)/, '')}
          </Avatar>
        </TableCell>
        <TableCell>{_.get(title, 'en', '')}</TableCell>
        {_.map(keys, (keyObj, key) => {
          const { prefix, format } = keyObj;
          const value = _.get(stats, key);
          return (
            <DenseTableCell
              style={{
                color: getColorByScoreType(value, _.get(minmax, `${key}.min`), _.get(minmax, `${key}.max`), key),
              }}
            >{`${prefix}${numeral(value).format(format)}`}</DenseTableCell>
          )
        })}
      </StyledTableRow>
    );
  }
});


const mapStateToProps = (state, ownProps) => {

  const mapReducer = _.get(state, 'domain.map');
  const { query } = mapReducer;
  const radius = mapReducer.analysisStationRadius || 600;

  const stations = _.filter(_.get(state, 'entities.pointOfInterests.entities'), poi => _.includes(['BTS', 'MRT'], poi.type));
  const projectEntities = _.get(state, 'entities.projects.entities');
  const filteredProjects = filterProjectsByQuery(projectEntities, query);
    // console.log({ allProjects, projects, ownProps });
  const allFacilities = _.get(state, 'entities.facilities.entities');

  const stationsWithProjects = _.chain(stations)
    .take(100)
    .map(station => {
      const lat = _.get(station, 'location.position._lat');
      const lng = _.get(station, 'location.position._long');
      const projectsNearStation = getProjectWithinRadius(filteredProjects, { lat, lng }, 0, radius);

      const facilityScores = calculateFacilityScore(allFacilities, lat, lng);
      
      return {
        ...station,
        projects: projectsNearStation,
        stats: {
          ...getProjectsStats(projectsNearStation),
          facilityScore: _.sumBy(facilityScores, score => score.value) / _.size(facilityScores),
        }, 
        radius,
      }
    })
    .filter(station => _.size(station.projects) > 0)
    .value();

  return {
    stations: stationsWithProjects,
  };
}

export default connect(mapStateToProps)(class extends React.Component {
  state = {
    search: '',
    orderBy: 'salePrice',
    order: 'desc',
  }

  fuse = null;

  componentDidMount = () => {
    this.fuse = new Fuse(this.props.stations, fuseOptions);
  }

  shouldComponentUpdate = (nextProps, nextState) => {
    return !_.isEqual(nextProps, this.props) || !_.isEqual(nextState, this.state);
  }

  componentDidUpdate = (prevProps) => {
    if (!_.isEqual(prevProps.stations, this.props.stations)) {
      this.fuse = new Fuse(this.props.stations, fuseOptions);
    }
  }
  
  createSortHandler = property => event => {
    if (this.state.orderBy === property) {
      this.setState({ order: this.state.order === 'desc' ? 'asc' : 'desc' });
    } else {
      this.setState({
        orderBy: property,
        order: 'desc',
      });
    }
  };
  handleChangeSearch = e => this.setState({ search: e.target.value })
  // handleSelectStation = (stationId) => {
  //   const { dispatch } = this.props;
  //   console.log('stationId', stationId);
  //   dispatch({ type: 'DOMAIN/MAP/ANALYSIS/HILIGHT_STATION/CHANGE', stationId });
  // }

  // handleClearSelectStation = (e) => {
  //   const { dispatch } = this.props;
  //   dispatch({ type: 'DOMAIN/MAP/ANALYSIS/HILIGHT_STATION/CLEAR' });
  // }

  render() {
    const { stations } = this.props;
    console.warn('Summary::Analysis');
    const { search, order, orderBy } = this.state;
    const minmax = _.reduce(stations, (acc, station) => {
      _.forIn(station.stats, (value, key) => {
        if (value < _.get(acc, `${key}.min`, 99999999)) _.set(acc, `${key}.min`, value);
        if (value > _.get(acc, `${key}.max`, 0)) _.set(acc, `${key}.max`, value);
      });
      return acc;
    }, {});

    const keys = {
      'salePrice': { label: 'Sale Price', prefix: '฿', format: '0,0a' },
      'capitalGain': { label: 'Capital Gain', prefix: '', format: '0.00%' },
      'startingPrice': { label: 'Starting Price', prefix: '฿', format: '0,0a' },
      'rentPrice': { label: 'Rent Price', prefix: '฿', format: '0,0.00' },
      'rentalYield': { label: 'Rental Yield', prefix: '', format: '0.00%' },
      'facilityScore': { label: 'Facility Score', prefix: '', format: '0.00%' },
    }
    
    const filteredStations = (search !== '' && !_.isEmpty(this.fuse)) ? this.fuse.search(search) : stations;

    const orderedStations = _.orderBy(filteredStations, [`stats.${this.state.orderBy}`], [this.state.order]);
    // console.log('orderedStations', orderedStations, this.state);
    return (
      <div style={{ paddingTop: 80 }}>
        <Grid container>
          <Grid item sm={6}>

          </Grid>
          <Grid item sm={6} style={{ textAlign: 'right' }}>
            <TextField variant="outlined" placeholder="Search" onChange={this.handleChangeSearch} margin="dense" />
          </Grid>
        </Grid>
        <Table padding="dense" style={{ fontSize: '0.8em' }} __onMouseOut={this.handleClearSelectStation}>
          <TableHead>
            <TableRow>
              <TableCell>Code</TableCell>
              <TableCell>Title</TableCell>
              {_.map(keys, (row, id) => {
                return (
                  <DenseTableCell
                    key={id}
                    numeric={row.numeric}
                    padding={row.disablePadding ? 'none' : 'default'}
                    sortDirection={orderBy === id ? order : false}
                  >
                    <Tooltip
                      title={`Sort by ${row.label}`}
                      placement={row.numeric ? 'bottom-end' : 'bottom-start'}
                      enterDelay={300}
                    >
                      <TableSortLabel
                        active={orderBy === id}
                        direction={order}
                        onClick={this.createSortHandler(id)}
                      >
                        {row.label}
                      </TableSortLabel>
                    </Tooltip>
                  </DenseTableCell>
                );
              })}
            </TableRow>
          </TableHead>
          <TableBody>
            {_.map(orderedStations, station => {
              return <StationTableRow {...station} keys={keys} minmax={minmax} />;
            })}
          </TableBody>
        </Table>
      </div>
    );
  }
});
