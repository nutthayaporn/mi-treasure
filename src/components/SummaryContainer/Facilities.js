import React from "react";
import styled from "styled-components";
import _ from "lodash";
import numeral from 'numeral';
import { Radar, RadarChart, PolarGrid, PolarAngleAxis} from 'recharts';
import { connect } from 'react-redux';

import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import Tab from '@material-ui/core/Tab';
import Tabs from '@material-ui/core/Tabs';
import { withStyles } from '@material-ui/core/styles';

import { calculateFacilityScore } from '../../utils/facilities-utils';

const orange = '#f06027';

const Contanier = styled.div`
  .box-card {
    margin-bottom: 10px;
    box-shadow: 0 4px 6px 0 rgba(0, 0, 0, 0.1);
  }
  .box-card-content {
    display: flex;
    flex-direction: column;
    justify-content: flex-start;
  }
  .box-card-title{
    flex-basis: 40%;
    flex-shrink: 0;
  }
  .card-title {
    display: flex;
    align-items: center;
    font-size: 18px;
    font-weight: bold;
    line-height: 1.22;
    color: #303030;
    margin-bottom: 10px;
    img {
      margin-right: 10px;
    }
  }
  .box-card-detail {
    display: flex;
    align-items: flex-start;
    flex-direction: column;
    justify-content: center;
  }
  .detail-name {
    color: #787878;
    padding: 0;
    font-size: 12px;
    display: flex;
    width: 100%;
    padding: 6px 0;
    justify-content: space-between;

    .detail-name-title {
      padding-bottom: 6px 0;
    }
  }
  .box-card-overall {
    margin-top: 22px;
    margin-bottom: 22px;
    box-shadow: 0 4px 6px 0 rgba(0, 0, 0, 0.1);
    position: relative;
    .overall {
      font-size: 24px;
      font-weight: bold;
      color: #ffffff;
      height: 68px;
      display: flex;
      .overall-title {
        border-top-left-radius: 4px;
        background-color: ${orange};
        -webkit-print-color-adjust: exact;
        flex-basis: 80%;
        display: flex;
        justify-content: flex-start;
        padding-left: 20px;
        align-items: center;
      }
      .overall-score {
        border-top-right-radius: 4px;
        background-color: ${orange};
        -webkit-print-color-adjust: exact;
        opacity: 0.7;
        flex-basis: 20%;
        display: flex;
        justify-content: center;
        align-items: center;
        text-align: center;
        flex-direction: column;
        line-height: 1;
        small {
          font-size: 18px;
          font-weight: bold;
        }
      }
    }
    .add-compare {
      position: absolute;
      bottom: 0;
      background-color: #f7f7f7;
      border-bottom-right-radius: 3px;
      border-bottom-left-radius: 3px;
      font-size: 14px;
      font-weight: 500;
      color: ${orange};
      padding: 10px 15px;
      width: 100%;
      display: flex;
      align-items: center;
    }
  }
  .line-chart{
    .box-place{
      display: flex;
      align-items: center;
      margin-bottom: 16px;
    }
    img{
        height: max-content;
        margin-right: 15px;
      }
    .box-place-title{
      width: 100%;
      .place-title{
        width: 100%;
        font-size: 16px;
        font-weight: 500;
        line-height: 1.57;
        color: #666666;
        display: flex;
        justify-content: space-between;

        .score {
          opacity: 0.5;
        }
      }
    }
    .bar-progress{
      width: 100%;
      position: relative;
      .bar-bg{
        width: 100%;
        height: 7px;
        border-radius: 10px;
        background-color: #E7E7E7;
      }
      .bar-color{
        position: absolute;
        top: 0;
        left: 0;
        width: 50%;
        height: 7px;
        border-radius: 10px;
        background-color: #9e64eb;
        -webkit-print-color-adjust: exact;
      }
    }
  }
`;

const styles = {
  root: {
    background:'#e7e7e7',
    height: 7,
    borderRadius: '100px'
  },
  barColorPrimary: {
    backgroundColor: '#676398'
  }
};

const StyledTabs = withStyles({
  indicator: {
    top: 0,
  },
  textColorInherit: {
    color: '#121212',
  }
})(Tabs);

const StyledTab = withStyles({
  selected:{
    background: '#FAFAFA',
    color: '#f06027',
    fontWeight: 500,
  },
})(Tab);

const mapStateToProps = (state, ownProps) => {
  const { radius } = ownProps;

  // Find current lat, long position
  const currentLat = ownProps.pointOfInterest.location.position._lat;
  const currentLong = ownProps.pointOfInterest.location.position._long;
  
  const allFacilities = _.get(state, 'entities.facilities.entities');

  const scores = calculateFacilityScore(allFacilities, currentLat, currentLong);

  // console.log('score', scores);
  return {
    overallScore: _.sumBy(scores, score => score.value) / _.size(scores),
    chartData: _.map(scores, score => ({ category: score.type, score: score.value * 100, fullMark: 100 })),
    scores,
  }
}

class Facilities extends React.PureComponent {

  state = {
    chart: 0,
  };

  handleChangeChart = (event, value) => {
    this.setState({ chart: value });
  };

  render() {
    const lineChartColor = {
      hospital: '#e47553',
      office: '#2ed0e6',
      shopping: '#ff9500',
      education: '#3e93d9',
      embassy: '#9e64eb',
      park: '#b5e62e'
    }

    const { chart } = this.state;
    const { scores, overallScore, pointOfInterest, getIconUrl, chartData } = this.props;
    // const { overrallScore, groupCateogries, dataChart, getIconUrl, pointOfInterest, groupedNearbyFacilities } = this.props;
    const name = _.get(pointOfInterest, 'title.th', 'Custom Point');
    return (
      <Contanier>
        <Card className="box-card-overall">
          <div className="overall">
            <span className="overall-title">Overrall Score</span>
            <span className="overall-score">
              {numeral(100 * overallScore).format('0')}
              <small>scores</small>
            </span>
          </div>
          <StyledTabs
            value={chart}
            onChange={this.handleChangeChart}
            indicatorColor="primary"
            textColor="primary"
          >
            <StyledTab label="Line Chart" />
            <StyledTab label="Spider Chart" />
          </StyledTabs>
          <CardContent style={{ minHeight: '100px', paddingBottom: '70px' }}>
            {chart === 0 &&
              <div className="line-chart">
              {_.map(scores, (item)=> {
                return (
                  <div className="box-place">
                    <img src={getIconUrl(item.type)} width={24} alt="facility-logo" />
                    <div className="box-place-title">
                      <div className="place-title">
                        <div>{_.upperFirst(item.type)}</div>
                        <div className="score">({numeral(100 * item.value).format('0')})</div>
                      </div>
                      <div className="bar-progress">
                        <div className="bar-bg" />
                        <div
                          className="bar-color"
                          style={{ backgroundColor: lineChartColor[item.type], width: `${100 * item.value}%` }}
                        />
                      </div>
                    </div>
                </div>
                )
              })}
              </div>
            }
            {chart === 1 &&
              <div className="spider-chart">
                <RadarChart width={435} height={360} data={chartData}>
                  <PolarGrid />
                  <PolarAngleAxis dataKey="category" />
                  <Radar name="category" dataKey="score" stroke={orange} fill={orange} fillOpacity={0.6}/>
                </RadarChart>
              </div>
            }
          </CardContent>
          <div className="add-compare">Add {name} to Compare</div>
        </Card>
        <div>
          {_.map(scores, ({ value, items, type }) => {
            if (_.size(items) === 0) return <div />;
            return (
              <Card key={type} className="box-card">
                <CardContent>
                  <div className="box-card-content">
                    <div className="box-card-title">
                      <div className="card-title">
                        <img src={getIconUrl(type)} width={24} alt="facility-logo" />{" "}
                        {_.upperFirst(type)}
                      </div>
                    </div>
                    <div className="box-card-detail">
                      {_.map(items, item => {
                        return (
                          <div className="detail-name" style={_.size(item) > 1 ? {'paddingBottom': '12px'} : {'paddingBottom': '0'}} key={item.id}>
                            <div className="detail-name-title">{item.title.en}</div>
                            <div>{`${numeral(item.distance * 1000).format('0,0')}m`}</div>
                          </div>
                        )
                      })}
                    </div>
                  </div>
                </CardContent>
              </Card>
            );
          })}
        </div>
      </Contanier>
    );
  }
}

export default connect(
  mapStateToProps,
)(withStyles(styles)(Facilities));

