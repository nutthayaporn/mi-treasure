import React from 'react';
import Fuse from 'fuse.js';

import _ from 'lodash';
import styled, { css } from 'styled-components';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import Badge from '@material-ui/core/Badge';
import TextField from '@material-ui/core/TextField';

import { addAllToCompare, removeAllFromCompare } from '../../actions/compare-actions';

import ProjectCardA from '../ProjectCard';
import ProjectCardB from '../ProjectCardB';

const BREAKPOINT = 960;

const Container = styled.div``;

const HeaderWrapper = styled.div`
  padding: 12px 12px 0;
  display: flex;
  justify-content: space-between;
`;

const ProjectsWrapper = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: nowrap;
  padding-top: 16px;
  padding-bottom: 26px;

  /* @media (min-width: ${BREAKPOINT}px) { */
    padding-left: 12px;
    padding-right: 12px;
  /* } */

  > div {
    flex: 1 0 396px;
    ${props => props.layout === 'A' &&
      `margin: 0 8px;`
    }
    ${props => props.layout === 'B' &&
      `margin-bottom: 8px;`
    }
  }

  ${props => props.layout === 'B' &&
    css`
      flex-direction: column;
      > div {
        flex: 1 0 auto;
        width: 100%;
      }
    `
  }
`;


const fuseOptions = {
  shouldSort: true,
  threshold: 0.2,
  location: 0,
  distance: 100,
  maxPatternLength: 32,
  minMatchCharLength: 1,
  keys: [
    "title.en",
    "title.th"
  ]
};

const mapStateToProps = (state, ownProps) => {
  // Domain
  const mapReducer = _.get(state, 'domain.map');
  const designProjectCard = _.get(state, 'domain.design.projectCard');
  const { visibleOnMapProjects } = mapReducer;

  return {
    ...ownProps,
    userSelectedProjects: visibleOnMapProjects,
    layout: state.domain.design.layout,
    compareProjectIds: state.domain.compare.ids,
    designProjectCard,
  }
}

const mapDispatchToProps = dispatch => {
  return {
    clickAddAllToCompare: (ids) => dispatch(addAllToCompare(ids)),
    clickRemoveAllFromCompare: () => dispatch(removeAllFromCompare()),
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(class extends React.PureComponent {
  static defaultProps = {
    projects: [],
  }

  state = {
    search: '',
    openMenu: false,
  }

  componentDidMount = () => {
    this.fuse = new Fuse(this.props.projects, fuseOptions);
  }

  shouldComponentUpdate = (nextProps, nextState) => {
    return !_.isEqual(nextProps, this.props) || !_.isEqual(nextState, this.state);
  }

  componentDidUpdate = (prevProps) => {
    if (!_.isEqual(prevProps.projects, this.props.projects)) {
      this.fuse = new Fuse(this.props.projects, fuseOptions);
    }
  }
  
  openMenu = (e) => {
    e.preventDefault();
    this.setState({ openMenu: true });
  }
  handleCloseMenu = (e) => {
    e.preventDefault();
    this.setState({ openMenu: false });
  }

  handleChangeSearch = e => this.setState({ search: e.target.value })

  // getDesignProjectCard = (type) => {
  //   switch (type) {
  //     case 'A': return ProjectCardA;
  //     case 'B': return ProjectCardB;
  //     default : return <div />;
  //   }
  // }

  render() {
    const { layout, projects, pointOfInterest, userSelectedProjects, onSelectProject, poi, compareProjectIds } = this.props;
    const { search } = this.state;
    const filteredProjects = (search !== '' && !_.isEmpty(this.fuse)) ? this.fuse.search(search) : projects;


    // const { designProjectCard } = this.props;
    // const ProjectCard = this.getDesignProjectCard(designProjectCard); 
    return (
      <Container>
        <HeaderWrapper>
          {_.size(compareProjectIds) > 0 && 
            <Badge color="primary" badgeContent={_.size(compareProjectIds)}>
              <Button
                color="primary"
                component={Link}
                target="_blank"
                to={`/compare/${poi}/${_.join(compareProjectIds, ',')}`}
                style={{ display: 'block', width: '100%', background: 'white', fontSize: '1rem', boxShadow: '0 4px 6px 0 rgba(0, 0, 0, 0.05)' }}
              >
                <span className="icon-compare" /> Compare
              </Button>
            </Badge>
          }
        </HeaderWrapper>
        <ProjectsWrapper layout={layout}>
          <Grid container>
            <Grid item xs={6}>
              <TextField label="Search" variant="outlined" fullWidth onChange={this.handleChangeSearch} margin="dense" />
            </Grid>
          </Grid>
          {_.map(projects, (project, index) =>
            <ProjectCardB
              key={project.id}
              {...project}
              reference={index + 1}
              hide={!_.some(filteredProjects, p => p.id === project.id)}
              pointOfInterest={pointOfInterest}
              onChange={onSelectProject}
              checked={_.includes(userSelectedProjects, project.id)}
            />
          )}
        </ProjectsWrapper>
      </Container>
    );
  }
});