import styled from 'styled-components';

export default styled.div`
  display: flex;
  flex-direction: row;
  a {
    text-decoration: none;
  }
`
