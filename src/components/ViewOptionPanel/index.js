import React from 'react';
import styled from 'styled-components';
import Checkbox from '@material-ui/core/Checkbox';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Divider from '@material-ui/core/Divider';

const projectCheckboxes = [
  { value: 'yearComplete', label: 'Year Complete' },
  { value: 'salePrice', label: 'Avg. price/sq.m.' },
  { value: 'rentPrice', label: 'AVg. rental price/sq.m.' },
  { value: 'capitalGain', label: 'Capital gain' },
  { value: 'rentalYield', label: 'Rental Yield' },
]

const LegendWrapper = styled.div`
  font-size: 11px;
  color: #787878;
  margin: 0 8px;
  display: flex;
  align-items: center;
`;
const ColorBox = styled.div`
  display: inline-block;
  width: 14px;
  height: 14px;
  margin-right: 5px;
  background: ${props => props.color || '#ececec'};
`;

const Legend = ({ label, color }) => (
  <LegendWrapper>
    <ColorBox color={color} />
    <span>{label}</span>
  </LegendWrapper>
)

export default class extends React.Component {

  state = {
    checked: [],
  }

  handleToggle = value => () => {
    const { checked } = this.state;
    const currentIndex = checked.indexOf(value);
    const newChecked = [...checked];

    if (currentIndex === -1) {
      newChecked.push(value);
    } else {
      newChecked.splice(currentIndex, 1);
    }

    this.setState({
      checked: newChecked,
    });

    this.props.onVisibleListUpdate(newChecked);
  };


  render() {
    const { checked } = this.state;
    return (
      <div>
        <List dense>
          {projectCheckboxes.map(item => (
            <ListItem
              button
              key={item.value}
              onClick={this.handleToggle(item.value)}
            >
              <Checkbox
                style={{ width: 25, height: 25 }}
                checked={checked.indexOf(item.value) !== -1}
                tabIndex={-1}
                disableRipple
              />
              <ListItemText
                primary={item.label}
              />
            </ListItem>
          ))}
          <Divider />
          <ListItem>
            <Legend label="Incomplete" color="#F7F719" />
          </ListItem>
          <ListItem>
            <Legend label="1-5 years" color="#E36C08" />
          </ListItem>
          <ListItem>
            <Legend label="6-10 years" color="#4BACC5" />
          </ListItem>
          <ListItem>
            <Legend label="More than 10 years" color="#7F7F7F" />
          </ListItem>
        </List>
      </div>
    );
  }
}