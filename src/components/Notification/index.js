import Notifications, { notify } from 'react-notify-toast';

export {
  notify,
}

export default Notifications;