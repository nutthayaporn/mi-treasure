import React from "react";
import PT from "prop-types";
import _ from "lodash";
import styled from "styled-components";
import { withStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Tag from '../Tag';
import * as Color from "../Color";
import { H5, H6 } from "../Typography";

const styles = {
  card: {
    maxWidth: 287,
    boxShadow: '0 5px 10px 0 rgba(0, 0, 0, 0.05)'
  },
  media: {
    width: "100%",
    height: 215
  },
  cardContent: {
    paddingLeft: 24,
    paddingRight: 24,
    paddingBottom: 32
  }
};
// paddingTop: '56.25%', // 16:9

const CardHolder = styled.button`
  outline: none;
  border: none;
  background-color: transparent;
  transition: 0.3s all ease-in-out;
  cursor: pointer;

  &.active {
    opacity: 0.5;
    transform: scaleY(1.5);
  }
  &.hover {
    opacity: 0.5;
    transform: scaleY(1.5);
  }
`;
const HeadLine = styled(H5)`
  color: #303030 !important;
  text-align: left;
`;

const Title = styled(H6)`
  font-size: 0.857rem !important;
  line-height: 1.57 !important;
  height: 85px;
  display: block;
  display: -webkit-box;
  max-width: 239px;
  -webkit-line-clamp: 4;
  -webkit-box-orient: vertical;
  overflow: hidden;
  text-overflow: ellipsis;
  text-align: left;
`;

const Date = styled(H6)`
  color: ${Color.primary} !important;
  font-size: 0.75rem !important;
  text-align: left;
`;

const TagHolder = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: flex-end;
  align-items: center;
`;

class NewsCard extends React.Component {
  static propTypes = {
    content: PT.shape({
      date: PT.string,
      head: PT.string,
      title: PT.string,
      tag: PT.oneOf(['articles', 'reviews']),
      img: PT.string
    }),
    classes: PT.objectOf(),
  };

  static defaultProps = {
    content: {
      date: "29 January 2018",
      head: "สรุปคอนโดมาแรง 2017",
      title:
        "ในปี 2017 ที่ผ่านมา กระแสการลงทุนคอนโดยังคงคึกคักอย่างต่อเนื่อง แม้ว่าอาจมีข่าวบางกระแสที่บอกว่าคอนโดล้นตลาด แต่เมื่อพิจารณาจากข้อมูลงานวิจัยของ AREA ในไตรมาส 4 ปี 2017 จะเห็นว่า มีอัตราเฉลี่ยการอยู่อาศัยจริงทั้งตลาด City condo สูงประมาณ 61% ของจำนวนยูนิตทั้งหมด นั่นหมายถึง คอนโดส่วนใหญ่เป็นเจ้าของอยู่เองเป็นหลัก ส่วนการปล่อยเช่าคอนโดของนักลงทุนยังมีไม่มากนัก ในอัตราเฉลี่ยเพียง 20% ของจำนวนยูนิตทั้งหมด",
      tag: "articles",
      img: "https://source.unsplash.com/random/400x300"
    }
  };
  getColor = (type) => {
    switch (type){
      case 'articles': return Color.Tag_Article;
      case 'reviews': return Color.Tag_Review;
      default:
      return '#ffff';
    }

  }
  render() {
    const { classes, onClick, content } = this.props;
    const { date, head, title, tag, img } = content;
    return (
      <CardHolder onClick={onClick}>
        <Card className={classes.card}>
          <CardMedia className={classes.media} image={img} title={head} />
          <CardContent classes={classes.cardContent}>
            <TagHolder>
              <Tag text={tag} />
            </TagHolder>
            <Date>{date}</Date>
            <HeadLine>{head}</HeadLine>
            <Title>{title}</Title>
          </CardContent>
        </Card>
      </CardHolder>
    );
  }
}

export default withStyles(styles)(NewsCard);
