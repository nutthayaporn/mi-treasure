import React from 'react';
import _ from 'lodash';
import { NavLink } from 'react-router-dom';
import { findDOMNode } from 'react-dom';

import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';

import Checkbox from '@material-ui/core/Checkbox';

import styled from 'styled-components';
import Button from '@material-ui/core/Button';
import Popover from '@material-ui/core/Popover';

const FloatingButtonBar = styled.div`
  position: absolute;
  z-index: 1;
  top: 20px;
  left: 50%;
  margin-left: -200px;
  width: 400px;
  display: flex;

  > * {
    flex-grow: 1;
  }
`;

const projectCheckboxes = [
  { value: 'yearComplete', label: 'Year Complete' },
  { value: 'salePrice', label: 'Avg. price/sq.m.' },
  { value: 'rentPrice', label: 'AVg. rental price/sq.m.' },
  { value: 'capitalGain', label: 'Capital gain' },
  { value: 'rentalYield', label: 'Rental Yield' },
]

export default class extends React.Component {

  static defaultProps = {
    checked: [],
  }
  state = {
    open: false,
    anchorEl: null,
    checked: this.props.checked,
  }

  componentWillReceiveProps = (nextProps) => {
    if (_.isEqual(nextProps.checked, this.props.checked)) {
      this.setState({
        checked: nextProps.checked,
      });
    }
  }

  handleClickProjectButton = (e) => {
    this.setState({
      open: true,
      anchorEl: findDOMNode(this.projectButton),
    });
  }

  handleClose = () => {
    this.setState({
      open: false,
    });
  }

  handleToggle = value => () => {
    const { checked } = this.state;
    const currentIndex = checked.indexOf(value);
    const newChecked = [...checked];

    if (currentIndex === -1) {
      newChecked.push(value);
    } else {
      newChecked.splice(currentIndex, 1);
    }

    this.setState({
      checked: newChecked,
    });

    this.props.onVisibleListUpdate(newChecked);
  };

  render() {
    const { open, anchorEl, checked } = this.state;
    return (
      <FloatingButtonBar>
        <Button
          variant="raised"
          // activeStyle={{ color: 'white', background: '#EB6323' }}
          ref={c => (this.projectButton = c)}
          onClick={this.handleClickProjectButton}>Projects</Button>
        <Button
          variant="raised"
          activeStyle={{ color: 'white', background: '#EB6323' }}
          component={NavLink}
          to="/meerkat/real-estate/bangkok/facilities">Facilities</Button>
        <Button
          variant="raised"
          activeStyle={{ color: 'white', background: '#EB6323' }}
          component={NavLink}
          to="/meerkat/real-estate/bangkok/analysis">Analysis</Button>
        <Popover
          open={open}
          anchorEl={anchorEl}
          onClose={this.handleClose}
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'left',
          }}
        >
          <List dense>
            {projectCheckboxes.map(item => (
              <ListItem
                button
                key={item.value}
                onClick={this.handleToggle(item.value)}
              >
                <Checkbox
                  style={{ width: 25, height: 25 }}
                  checked={checked.indexOf(item.value) !== -1}
                  tabIndex={-1}
                  disableRipple
                />
                <ListItemText
                  primary={item.label}
                />
              </ListItem>
            ))}
          </List>
        </Popover>
      </FloatingButtonBar>
    );
  }
}