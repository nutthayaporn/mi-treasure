import React from 'react';
import styled from 'styled-components';
import IconDone from '@material-ui/icons/Done';
import List from '@material-ui/core/List';
import ListSubheader from '@material-ui/core/ListSubheader';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';

import { connect } from 'react-redux';


const items = [
  // {
  //   key: 'layout',
  //   name: 'Layout',
  //   options: ['A', 'B']
  // },
  {
    key: 'projectCard',
    name: 'Project Card',
    options: ['A', 'B']
  }
]

// const Container = styled.div`
//   position: fixed;
//   left: 0;
//   top: 50%;
//   transform: translateY(-50%);

//   @media print {
//     display: none;
//   }
// `;

export default connect(
  state => ({ design: state.domain.design }),
  dispatch => ({
    onSelectItem: (key, value) => dispatch({ type: 'DESIGN/CHANGE', key, value }),
  })
)(({ design, onSelectItem }) => (
  <React.Fragment>
    {items.map(item => (
      <List key={item.key} component="nav" dense disablePadding subheader={<ListSubheader component="div">{item.name}</ListSubheader>}>
        {item.options.map(option => (
          <ListItem key={option} button onClick={() => onSelectItem(item.key, option)}>
            {design[item.key] === option &&
              <ListItemIcon>
                <IconDone />
              </ListItemIcon>
            }
            <ListItemText inset primary={option} />
          </ListItem>
        ))}
      </List>)
    )}
  </React.Fragment>
));
