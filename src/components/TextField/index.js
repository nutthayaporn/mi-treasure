import React from 'react';
import TextField from '@material-ui/core/TextField';

export default (props) => (
  <TextField
    InputLabelProps={{
      shrink: true,
      style: {
        color: '#EB6323',
      }
    }}
    {...props}
  />
)
