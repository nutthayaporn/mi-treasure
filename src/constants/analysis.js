export default {
  'salePrice': { label: 'Sale Price', prefix: '฿', format: '0,0a' },
  'capitalGain': { label: 'Capital Gain', prefix: '', format: '0.00%' },
  'startingPrice': { label: 'Starting Price', prefix: '฿', format: '0,0a' },
  'rentPrice': { label: 'Rent Price', prefix: '฿', format: '0,0.00' },
  'rentalYield': { label: 'Rental Yield', prefix: '', format: '0.00%' },
  'facilityScore': { label: 'Facility Score', prefix: '', format: '0.00%' },
}