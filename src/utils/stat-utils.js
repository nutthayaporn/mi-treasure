// import moment from 'moment';
// import _ from 'lodash';

// const getSalePriceByYear = (year, history) => {
//   const record = _.find(history, record => record.time === year);
//   return _.get(record, 'salePrice', 0);
// }
// const getCapitalGainFromTo = (from, to) => {
//   return (from - to) / to;
// }

// export const calcProjectCapitalGain = project => {
//   const { history, current } = project;
//   const { salePrice: priceToday } = current;
  
//   const today = moment();
//   const last1Y = today.clone().subtract(1, 'years').format('YYYY');
//   const last3Y = today.clone().subtract(3, 'years').format('YYYY');
//   const last5Y = today.clone().subtract(5, 'years').format('YYYY');
//   // console.log('last1Y', last1Y);
//   // console.log('last3Y', last3Y);
//   // console.log('last5Y', last5Y);

//   const priceLast1Y = getSalePriceByYear(last1Y, history);
//   const priceLast3Y = getSalePriceByYear(last3Y, history);
//   const priceLast5Y = getSalePriceByYear(last5Y, history);
//   // console.log('priceLast1Y', priceLast1Y);
//   // console.log('priceLast3Y', priceLast3Y);
//   // console.log('priceLast5Y', priceLast5Y);

//   const cap1Y = getCapitalGainFromTo(priceToday, priceLast1Y);
//   const cap3Y = getCapitalGainFromTo(priceToday, priceLast3Y);
//   const cap5Y = getCapitalGainFromTo(priceToday, priceLast5Y);

//   // console.log('cap1Y', cap1Y);
//   // console.log('cap3Y', cap3Y);
//   // console.log('cap5Y', cap5Y);
//   // const capitalGain = (cap1Y + (cap3Y / 3) + (cap5Y / 5)) / 3;
  
//   const realValues = _.compact([cap1Y, cap3Y/3, cap5Y/5]);
//   const capitalGain = _.sum(realValues) / _.size(realValues);
//   return Math.round(10000 * capitalGain) / 10000;
// }


export const getProjectStat = project => {

  const { current, startingPrice, yearComplete, startingPersqm } = project;
  const { salePrice, rentPrice, capitalGain, rentalYield } = current;

  return {
    startingPersqm,
    yearComplete,
    salePrice,
    capitalGain,
    startingPrice,
    rentPrice,
    rentalYield,
  }
}