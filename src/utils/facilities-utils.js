import _ from 'lodash';
import { getDistance } from './map-utils';

export const cleanFacilities = data => {
  const allCateogries = _.map(data, item => {
    if (item.category) return item.category;
  });

  const cateogries = _.uniq(allCateogries);
  const groupCateogries = _.filter(cateogries, category => {
    if (category !== 'extra Group (ไม่มีผลในการคำนวน แสดงผลในแผนที่เฉยๆ)' && category !== undefined && category !== 'future-project') return category;
  });

  return {
    groupCateogries
  };
};

export const calculateFacilityScore = (facilities, lat, lng, treshold = 0.6) => {

  const facilitiesWithDistance = _.map(facilities, facility => {
    return { ...facility, distance: getDistance(lat, lng, _.toNumber(facility.location.lat), _.toNumber(facility.location.lon)) };
  });

  const nearbyFacilities = _.filter(facilitiesWithDistance, facility => facility.distance < treshold);

  const typesToShow = [
    'office',
    'education',
    'shopping',
    'hospital',
    'park'
  ];

  const scores = _.chain(typesToShow)
    .map(type => ({ type, items: _.filter(nearbyFacilities, f => f.category === type )}))
    .map(({ type, items }) => {
      const n = _.size(items);
      const RADIUS = 0.6;
      const WEIGHT = {
        shopping: 0.15,
        education: 0.05,
        park: 0.25,
        hospital: 0.25,
        transportation: 0.05,
        office: 0.05, 
      };

      const distanceEvaluation = n > 0 ? _.sumBy(items, f => (RADIUS - f.distance) / RADIUS) / n : 0;
      const countEvaluation = n / _.size(nearbyFacilities);
      const interestWeightScore = _.get(WEIGHT, type, 0.2);
      const overallScore = distanceEvaluation * countEvaluation * interestWeightScore;
      const maxValue = 1 * countEvaluation * interestWeightScore;
      const value = maxValue > 0 ? overallScore / maxValue : 0;
      return {
        type,
        n,
        items,
        distanceEvaluation,
        countEvaluation,
        interestWeightScore,
        maxValue,
        overallScore,
        value,
      };
    })
    .value();
  
  return scores;
}