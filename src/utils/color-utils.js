import { interpolateRgb } from 'd3-interpolate';

export const getMinMaxColorByScoreType = (scoreType) => {
  switch (scoreType) {
    case 'capitalGain':
    case 'rentalYield':
      return { min: '#9bedff', max: '#2a0e56' };
    default:
      return { min: '#4caf50', max: '#b23c17'};
  }
} 


export const getColorByScoreType = (value, min, max, scoreType) => {
  switch (scoreType) {
    case 'facilityScore':
      return getColor(value, min, max, '#b23c17', '#4caf50');
    case 'capitalGain':
    case 'rentalYield':
      return getColor(value, min, max, '#9bedff', '#2a0e56');
    default: return getColor(value, min, max, '#4caf50', '#b23c17');
  }
}

export const getColor = (value, min, max, colorStart, colorStop) => {
  const interpolator = interpolateRgb(colorStart, colorStop);
  const _value = (value - min) / (max - min);
  return interpolator(_value);
}