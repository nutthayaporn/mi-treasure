export const offsetPosition = (lat, lng, de = 0, dn = 0) => {
  //Earth’s radius, sphere
  const R = 6378137;

  //Coordinate offsets in radians
  const dLat = dn / R;
  const dLon = de / (R * Math.cos(Math.PI * lat/180));

  //OffsetPosition, decimal degrees
  const latO = lat + (dLat * 180 / Math.PI);
  const lonO = lng + (dLon * 180 / Math.PI);
  return { lat: latO, lng: lonO };
}

export const getDistance = (lat1, lon1, lat2, lon2, unit = 'K') => {
	const radlat1 = Math.PI * lat1/180;
	const radlat2 = Math.PI * lat2/180;
	const theta = lon1-lon2;
	const radtheta = Math.PI * theta/180;
	let dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
	dist = Math.acos(dist);
	dist = dist * 180/Math.PI;
	dist = dist * 60 * 1.1515;
	if (unit === "K") { dist = dist * 1.609344 }
	if (unit === "N") { dist = dist * 0.8684 }
	return dist;
}