import axios from 'axios';

export const updateProject = (projectId, data) => {
  return axios({
    url: `${process.env.REACT_APP_API}/admin/projects/${projectId}`,
    method: 'PUT',
    data,
  }).then(res => {
    return res;
  });
}

export const updateProjectHistory = (projectId, time, source, { salePrice, rentPrice, remark }) => {
  return axios({
    url: `${process.env.REACT_APP_API}/admin/projects/${projectId}/history/${time}/sources/${source}`,
    method: 'PUT',
    data: { salePrice, rentPrice, remark },
  }).then(res => {
    return res;
  });
}