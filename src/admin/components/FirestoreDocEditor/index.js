import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { notify } from 'react-notify-toast';
import _ from 'lodash';

import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Divider from '@material-ui/core/Divider';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';

import EditIcon from '@material-ui/icons/Edit';

// import MediaSelector from './MediaSelector';
import CreateDocButton from '../CreateDocButton';
import ConfirmDialog from '../ConfirmDialog';
import InputField from '../InputField';

// import {
//   createClient,
// } from 'forviz-mediajs';

const cleanObject = (obj) => {
  Object.keys(obj).forEach(key => {
    if (obj[key] && typeof obj[key] === 'object') cleanObject(obj[key]);
    else if (obj[key] === undefined) delete obj[key];
  });
  return obj;
};

const DRAWER_WIDTH = 500;

const PaperContent = styled.div`
  padding: 40px;
  box-sizing: border-box;
`;

const Section = styled.section`
  margin: 25px 0;
`;

const ActionBar = styled.div`
  box-sizing: border-box;
  padding: 20px;
  position: fixed;
  right: 0;
  bottom: 0;
  width: ${DRAWER_WIDTH}px;
  /* background: #fafafa; */
  border-top: 1px solid #ececec;
  background: rgba(30, 30, 30, 0.9);
  border-top: 1px solid rgba(255, 255, 255, 0.2);
  display: flex;
  justify-content: flex-end;
`;

export default class extends React.PureComponent {

  static propTypes = {
    data: PropTypes.any,
    mediaEngine: PropTypes.string, 
    mediaEngineConfig: PropTypes.object,
    mapDocDataToFields: PropTypes.func,
    mapFieldsToDocData: PropTypes.func,
    sections: PropTypes.array,
    getSections: PropTypes.func,
    onSave: PropTypes.func,
    onDelete: PropTypes.func,
    onCancel: PropTypes.func,
    onUploadFile: PropTypes.func,
    onDeleteFile: PropTypes.func,
  }

  static defaultProps = {
    data: null,
    mediaEngine: 'firebase',
    mediaEngineConfig: {},
    mapDocDataToFields: (data) => {
      return _.mapValues(data, (value, key) => ({ label: _.toUpper(key), value }));
    },
    mapFieldValuesToDocData: fieldValues => {
      return _.mapValues(fieldValues, field => field.value);
    },
    sections: undefined,
    getSections: fields => {
      return [{ title: 'Fields', fields: _.keys(fields) }];
    }
  }

  constructor(props) {
    super(props);
    // const { mapDocDataToFields } = props;
    this.state = {
      // fields: mapDocDataToFields(),
      docData: null,
      fieldValues: {},
    }

    this.show = notify.createShowQueue();
  }

  loadDoc = (docRef) => {
    if (docRef) {
      const { mapDocDataToFields } = this.props;
      this.unsubscribeDoc = docRef
        .onSnapshot(doc => {
          const fields = mapDocDataToFields(doc.data(), doc.id);
          this.setState({
            id: doc.id,
            docData: doc.data(),
            // fields,
            fieldValues: _.mapValues(fields, field => field.value),
          });
        });
    } else {
      const { id, data, mapDocDataToFields } = this.props;
      const fields = mapDocDataToFields(data, id);
      this.setState({
        id,
        docData: data,
        currentTab: 0,
        fieldValues: _.mapValues(fields, field => _.get(field, 'value')),
      })
    }
  }

  componentDidMount() {
    this.loadDoc(this.props.docRef);
  }

  componentDidUpdate(prevProps, prevState) {
    if (_.get(this.props, 'docRef.id') !== _.get(prevProps, 'docRef.id')) {
      this.loadDoc(this.props.docRef);
    }
  }

  componentWillUnmount() {
    if (typeof this.unsubscribeDoc === 'function') this.unsubscribeDoc();
  }

  save = async  (e) => {
    const { docRef, mapFieldValuesToDocData } = this.props;
    e.preventDefault();

    const doc = await docRef.get();
    // console.log('this.state.fieldValues', this.state.fieldValues);
    // const fields = mapDocDataToFields(this.state.docData);
    // const data = mapFieldsToDocData(fields, doc.id);
    const data = mapFieldValuesToDocData(this.state.fieldValues);
    
    const cleanedData = cleanObject(data);
    console.log('save', cleanedData);
    
    if (doc.exists) {
      docRef.update(cleanedData);      
    } else {
      docRef.set(cleanedData);
    }
    this.show('Saved', 'success', 4000);
    if (typeof this.props.onSave === 'function') this.props.onSave(docRef.id, this.state.fieldValues);
    if (typeof this.props.onClose === 'function') this.props.onClose();
  }

  deleteDoc = () => {
    // console.log('delete Doc');
    const { docRef, onClose, onDelete } = this.props;
    docRef.delete().then(() => {
      this.show('Deleted', 'success', 4000);
      if (typeof this.props.onDelete === 'function') onDelete(docRef.id);
      if (typeof this.props.onClose === 'function') onClose();
    });
  }

  handleInputChange = (name, eventOrValue) => {
    const value = _.get(eventOrValue, 'target.value', eventOrValue);
    // console.log('inputChange', name, value);
    this.setState({
      fieldValues: {
        ...this.state.fieldValues,
        [name]: value,
      },
    })
  }

  uploadFile = (media) => {
    console.log('uploadMedia', media);
    switch (this.props.mediaEngine) {
      case 'custom': {
        const { onUploadFile } = this.props;
        onUploadFile(media);
        break;
      }

      case 'firebase':
      default: {
        // const { mediaEngineConfig } = this.props;
        // const client = createClient(mediaEngineConfig);
        // console.log('uploadFile:mediaEngineConfig', mediaEngineConfig);
        // client.uploadToStorage(media.file)
        //   .then(file => {
        //     console.log('uploaded', file);
        //     // const filename = _.get(file, 'metadata.name');
        //     // const mediaId = _.kebabCase(_.replace(filename, /(.jpg|.png|.gif|.svg)/ig, ''));
        //     const mediaId = media.id;
        //     console.log('mediaId', mediaId);
            
        //     this.show(`Re-Uploaded ${mediaId} complete`, 'success', 4000);
        //     return client.saveMediaToFirestore(mediaId, file.ref.toString(), { title: media.title, tags: media.tags, url: file.url });
        //   })
        //   .then(mediaId => {
        //     this.show(`Update Media complete`, 'success', 4000);
        //   });
        break;
      }
    }
  }

  deleteFile = (mediaRef) => {
    // console.log('deleteFile', mediaRef);
    switch (this.props.mediaEngine) {
      case 'custom': {
        const { onDeleteFile } = this.props;
        onDeleteFile(mediaRef);
        break;
      }

      case 'firebase':
      default: {
        // const { mediaEngineConfig } = this.props;
        // const client = createClient(mediaEngineConfig);
        // client.getMediaFromRef(mediaRef)
        //   .then(media => {
        //     const storageRef = media.ref;
        //     return Promise.all([
        //       client.deleteMediaFromFirestore(mediaRef),
        //       client.deleteFromStorage(storageRef)
        //     ]);
        //   })
        //   .then(() => this.show(`Medias deleted`, 'success', 4000));
  
        break;
      }
    }
  }

  handleChangeID = async ({ newId }) => {
    const { docRef } = this.props;
    const data = await docRef.get().then(doc => {
      return doc.data();
    });
    
    docRef.parent.doc(newId).set(data)
      .then(() => {
        docRef.delete();
        this.show(`Successfully change Document ID ${docRef.id} to ${newId}`, 'success', 4000);

        // if (typeof this.props.onDelete === 'function') this.props.onDelete(docRef.id);
        if (typeof this.props.onClose === 'function') this.props.onClose();
      });
  }

  renderField = (field, fieldName) => {
    const value = this.state.fieldValues[fieldName];

    if (!field) return (<div />);

    // const { label, options, list, disabled, multiple } = field;
    // console.log('renderField', fieldName, value);
    return (
      <div key={`${fieldName}`} style={{ display: 'flex', padding: '6px 0', justifyContent: 'space-between', alignItems: 'center' }}>
        <InputField
          type={field.type || 'TextField'}
          {...field}
          value={value}
          onChange={e => this.handleInputChange(fieldName, e)}
          onUploadFile={this.uploadFile}
          onDeleteFile={this.deleteFile}
        />
      </div>
    );
  }

  render() {
    const { width, mapDocDataToFields, mapFieldValuesToDocData, docRef, sections, getSections, onClose, titleField = 'fields.name.en', renderTitle, allowEditId } = this.props;
    const { fieldValues } = this.state;
    const updatedData = mapFieldValuesToDocData(fieldValues);
    const fields = mapDocDataToFields(updatedData);
    const formSections = sections || getSections(fields);

    const id = docRef ? _.get(docRef, 'id', '') : this.props.id;
    const title = typeof renderTitle === 'function' ? renderTitle(updatedData) : _.get(updatedData, titleField, id);
    // console.log('fields', fields, titleField);
    return (
      <div style={{ height: '100vh', position: 'relative', overflow: 'scroll' }}>
        <Paper elevation={0} style={{ width, padding: '40px 0', boxSizing: 'border-box' }}>
          <PaperContent>
            {allowEditId &&
              <Grid container alignItems="center">
                <Grid item><Typography variant="caption" style={{ opacity: 0.5 }}>{id}</Typography></Grid>
                <Grid item>
                  <CreateDocButton
                    title="Change ID"
                    fields={[
                      { type: 'TextField', name: 'newId', label: 'New ID', required: true }
                    ]}
                    okText={`Change ID`}
                    renderButton={() => <IconButton><EditIcon fontSize="small" /></IconButton>}
                    onCreate={this.handleChangeID}
                  />
                </Grid>
              </Grid>
            }
            <Typography variant="h4">{title}</Typography>
            {_.map(formSections, section => (
              <Section key={section.title}>
                <Typography variant="subtitle1" style={{ marginBottom: 20 }}>{section.title}</Typography>
                <div>
                  {_.map(section.fields, fieldName => this.renderField(fields[fieldName], fieldName))}
                </div>
                <Divider />
              </Section>
            ))}
          </PaperContent>
        </Paper>
        <ActionBar style={{ width, zIndex: 10 }}>
          <div style={{ width: '100%', display: 'flex', justifyContent: 'space-between' }}>
            <ConfirmDialog
              title="Delete Document"
              content="This will permanently delete this document, are you sure?"
              onOk={this.deleteDoc}
              render={() => <Button style={{ color: '#ff1744' }}>Delete</Button>}
            />
            <div style={{ display: 'flex' }}>
              <Button onClick={onClose}>Cancel</Button>
              <Button color="primary" onClick={this.save}>Save</Button>
            </div>
          </div>
        </ActionBar>
      </div>
    );
  }
}
