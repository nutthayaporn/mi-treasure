import React, { Component } from "react";
import Slide from "@material-ui/core/Slide";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import Input from "@material-ui/core/Input";
import FormControl from "@material-ui/core/FormControl";
import FormLabel from "@material-ui/core/FormLabel";

import CircularProgress from "@material-ui/core/CircularProgress";

import { notify } from "../../components/Notification";
import db from "../../engine/db";

const Transition = props => {
  return <Slide direction="up" {...props} />;
};

class EditProfileDialog extends Component {
  static defaultProps = {
    onClose: () => null
  };

  state = {
    loading: false
  };

  componentDidMount() {
    const { uid, displayName } = this.props.user;
    const data = db.collection("users").doc(uid);
    data
      .get()
      .then(doc => {
        if (doc.exists) {
          const { name = displayName, phone = "", company = "" } = doc.data();
          this.nameInput.value = name;
          this.phoneInput.value = phone; 
          this.companyInput.value = company; 
          
        } else {
          console.log("No such document!");
        }
        return null;
      })
      .catch(error => {
        console.log("Error getting document:", error);
      });
  }

  handleClose = e => {
    this.props.onClose(true);
  };

  handleCancel = e => {
    this.handleClose(e);
  };

  save = e => {
    const { uid } = this.props.user;
    const name = this.nameInput.value;
    const phone = this.phoneInput.value;
    const company = this.companyInput.value;
    this.handleClose();
    this.setState({
      loading: true
    });
    db.collection("users")
      .doc(uid)
      .set({
        name,
        phoneNumber: phone,
        company
      })
      .then(() => {
        notify.show("Save successfully")
        console.log("Document successfully written!");
        this.setState({
          loading: false
        });
      })
      .catch((error) => {
        console.error("Error writing document: ", error);
        notify.show(`Error:${error.message}`, 'error')
      });
  };

  render() {
    const { loading } = this.state;
    return (
      <Dialog
        open={this.props.open}
        TransitionComponent={Transition}
        keepMounted
        onClose={this.handleClose}
        aria-labelledby="edit-profile"
        aria-describedby="edit-profile-description"
      >
        <DialogTitle id="edit-profile">Edit Profile</DialogTitle>
        <DialogContent>
          <div
            style={{
              display: "flex",
              flexDirection: "column",
              width: 500,
              maxWidth: "100%"
            }}
          >
            <FormControl component="fieldset" style={{ marginBottom: 12 }}>
              <FormLabel component="legend">Name</FormLabel>
              <Input
                autoFocus
                inputRef={node => (this.nameInput = node)}
                type="text"
                fullWidth
              />
            </FormControl>
            <FormControl component="fieldset" style={{ marginBottom: 12 }}>
              <FormLabel component="legend">Phone</FormLabel>
              <Input
                inputRef={node => (this.phoneInput = node)}
                type="text"
                fullWidth
              />
            </FormControl>
            <FormControl component="fieldset" style={{ marginBottom: 12 }}>
              <FormLabel component="legend">Company</FormLabel>
              <Input
                inputRef={node => (this.companyInput = node)}
                type="text"
                fullWidth
              />
            </FormControl>
          </div>
        </DialogContent>
        <DialogActions>
          <Button onClick={this.handleCancel} color="primary">
            Cancel
          </Button>
          <Button onClick={this.save} color="primary" disabled={loading}>
            {!loading ? "Save" : <CircularProgress size={24} />}
          </Button>
        </DialogActions>
      </Dialog>
    );
  }
}

export default EditProfileDialog;
