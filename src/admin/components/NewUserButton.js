import React from 'react';

import AddIcon from '@material-ui/icons/AddCircle';
import Slide from '@material-ui/core/Slide';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Input from '@material-ui/core/Input';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';

import CircularProgress from '@material-ui/core/CircularProgress';

import db from "../../engine/db";
import { createUser } from '../api/user';

const Transition = (props) => {
  return <Slide direction="up" {...props} />;
}

export default class extends React.PureComponent {
  static defaultProps = {
    defaultRole: 'mi-treasure.sale',
  }

  state = {
    open: false,
    loading: false,
    role: this.props.defaultRole,
  }

  handleClose = (e) => {
    this.setState({ open: false });
  }

  handleCancel = (e) => {
    this.handleClose(e);
  }

  handleChangeRole = (e) => {
    this.setState({
      role: e.target.value,
    });
  }

  save = (e) => {
    const email = this.emailInput.value;
    const password = this.passwordInput.value;
    const displayName = this.displayNameInput.value;
    const company = this.companyInput.value;
    const phone = this.phoneInput.value;
    const role = this.state.role;
    // this.props.onSave(email, password, displayName, role);
    this.setState({
      loading: true,
    });

    createUser(email, password, displayName, role, phone, company)
      .then(user => {
        console.log('createUser', user);
        this.setState({
          open: false,
          role: this.props.defaultRole,
        });
        this.emailInput.value = '';
        this.passwordInput.value = '';
        this.displayNameInput.value = '';
        this.phoneInput.value = '';
        this.companyInput.value = '';
        db.collection("users")
        .doc(user.uid)
        .set({
          name: displayName,
          phone,
          company
        })

        if (this.props.onSuccess) this.props.onSuccess(user);
      })
      .catch(error => {
        if (this.props.onError) this.props.onError(error);
      })
      .finally(() => {
        this.setState({ loading: false });
      })
  
  }
  render() {
    const { role, loading } = this.state;
    return (
      <React.Fragment>
        <Button
            variant="extendedFab" color="primary"
            onClick={() => this.setState({ open: true })}
          >
            <AddIcon style={{ marginRight: 8 }} /> Create User
          </Button>
        <Dialog
          open={this.state.open}
          TransitionComponent={Transition}
          keepMounted
          onClose={this.handleClose}
          aria-labelledby="alert-dialog-slide-title"
          aria-describedby="alert-dialog-slide-description"
        >
          <DialogTitle id="alert-dialog-slide-title">
            Add new user
          </DialogTitle>
          <DialogContent>
            <div style={{ display: 'flex', flexDirection: 'column', width: 500, maxWidth: '100%' }}>
              <FormControl component="fieldset" required style={{ marginBottom: 12 }}>
                <FormLabel component="legend">Email Address</FormLabel>
                <Input
                  autoFocus
                  inputRef={node => this.emailInput = node}
                  id="name"
                  type="email"
                  fullWidth
                />
              </FormControl>
              <FormControl component="fieldset" required style={{ marginBottom: 12 }}>
                <FormLabel component="legend">Password</FormLabel>
                <Input
                  inputRef={node => this.passwordInput = node}
                  id="password"
                  type="password"
                  fullWidth
                />
              </FormControl>
              <FormControl component="fieldset" required style={{ marginBottom: 12 }}>
                <FormLabel component="legend">Company</FormLabel>
                <Input
                  inputRef={node => this.companyInput = node}
                  id="company"
                  type="text"
                  fullWidth
                />
              </FormControl>
              <FormControl component="fieldset" required style={{ marginBottom: 12 }}>
                <FormLabel component="legend">Display Name</FormLabel>
                <Input
                  inputRef={node => this.displayNameInput = node}
                  id="displayName"
                  type="text"
                  fullWidth
                />
              </FormControl>
              <FormControl component="fieldset" required style={{ marginBottom: 12 }}>
                <FormLabel component="legend">Phone</FormLabel>
                <Input
                  inputRef={node => this.phoneInput = node}
                  id="phone"
                  type="text"
                  fullWidth
                />
              </FormControl>
              <FormControl component="fieldset" required style={{ marginBottom: 12 }}>
                <FormLabel component="legend">Role</FormLabel>
                <RadioGroup
                  aria-label="role"
                  name="role"
                  value={role}
                  onChange={this.handleChangeRole}
                >
                  <FormControlLabel value="mi-treasure.admin" control={<Radio />} label="Admin" />
                  <FormControlLabel value="mi-treasure.sale" control={<Radio />} label="Sale" />
                  <FormControlLabel value="mi-treasure.alliance" control={<Radio />} label="Alliance" />
                </RadioGroup>
              </FormControl>
            </div>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleCancel} color="primary">
              Cancel
            </Button>
            <Button onClick={this.save} color="primary" disabled={loading}>
              {!loading ? 'Create User' : <CircularProgress size={24} />}
            </Button>
          </DialogActions>
        </Dialog>
      </React.Fragment>

    );
  }
}