import React from 'react';
import _ from 'lodash';
import { withRouter, Link } from 'react-router-dom';
import { connect } from 'react-redux';

import deburr from 'lodash/deburr';
import keycode from 'keycode';
import Downshift from 'downshift';

import { withStyles } from '@material-ui/core/styles';
import { fade } from '@material-ui/core/styles/colorManipulator';

import Paper from '@material-ui/core/Paper';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import MenuItem from '@material-ui/core/MenuItem';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import InputBase from '@material-ui/core/InputBase';

import IconMenu from '@material-ui/icons/Menu';
import IconSearch from '@material-ui/icons/Search';


const styles = theme => ({
  title: {
    display: 'none',
    [theme.breakpoints.up('sm')]: {
      display: 'block',
    },
  },
  search: {
    position: 'relative',
    borderRadius: theme.shape.borderRadius,
    backgroundColor: fade(theme.palette.common.white, 0.15),
    '&:hover': {
      backgroundColor: fade(theme.palette.common.white, 0.25),
    },
    marginRight: theme.spacing.unit * 2,
    marginLeft: 0,
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      marginLeft: theme.spacing.unit * 3,
      width: 'auto',
    },
  },
  searchIcon: {
    width: theme.spacing.unit * 9,
    height: '100%',
    position: 'absolute',
    pointerEvents: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  inputRoot: {
    color: 'inherit',
    width: '100%',
  },
  inputInput: {
    paddingTop: theme.spacing.unit,
    paddingRight: theme.spacing.unit,
    paddingBottom: theme.spacing.unit,
    paddingLeft: theme.spacing.unit * 10,
    transition: theme.transitions.create('width'),
    width: '100%',
    [theme.breakpoints.up('md')]: {
      width: 200,
    },
  },
  sectionDesktop: {
    display: 'none',
    [theme.breakpoints.up('md')]: {
      display: 'flex',
    },
  },
  sectionMobile: {
    display: 'flex',
    [theme.breakpoints.up('md')]: {
      display: 'none',
    },
  },
  searchContainer: {
    position: 'relative',
  },
  paper: {
    position: 'absolute',
    zIndex: 1,
    marginTop: theme.spacing.unit,
    left: 0,
    right: 0,
    width: 400,
  },
});

const mapStateToProps = state => {
  return {
    suggestions: [
      ..._.map(state.entities.facilities.entities, (facility, id) => ({
        value: `/admin/facilities/${id}`,
        label: _.get(facility, 'title.en', ''),
      })),
      ..._.map(state.entities.projects.entities, (project, id) => ({
        value: `/admin/projects/${id}`,
        label: _.get(project, 'title.en', ''),
      }))
    ],
  }
}

function getSuggestions(suggestions, value) {
  const inputValue = deburr(value.trim()).toLowerCase();
  const inputLength = inputValue.length;
  let count = 0;

  return inputLength === 0
    ? []
    : suggestions.filter(suggestion => {
        const keep =
          count < 5 && suggestion.label.slice(0, inputLength).toLowerCase() === inputValue;

        if (keep) {
          count += 1;
        }

        return keep;
      });
}


export default withRouter(withStyles(styles)(connect(mapStateToProps)(class extends React.Component {

  handleSelectSearchItem = (selectedItem) => {
    const { history } = this.props;
    history.push(selectedItem.value);
  }
  render() {
    const { classes, suggestions } = this.props;
    return (
      <AppBar position="fixed">
        <Toolbar>
          <IconButton color="inherit" aria-label="Open drawer">
            <IconMenu />
          </IconButton>
          <Typography className={classes.title} variant="h6" color="inherit" noWrap>
            MI Treasure
          </Typography>
          <div className={classes.search}>
            <div className={classes.searchIcon}>
              <IconSearch />
            </div>
            {/* <InputBase
              placeholder="Search…"
              classes={{
                root: classes.inputRoot,
                input: classes.inputInput,
              }}
            /> */}
            <Downshift
              onChange={this.handleSelectSearchItem}
              itemToString={item => ''}
            >
              {({
                getInputProps,
                getItemProps,
                getMenuProps,
                highlightedIndex,
                inputValue,
                isOpen,
                selectedItem,
              }) => (
                <div className={classes.searchContainer}>
                  <InputBase
                    placeholder="Search…"
                    {...getInputProps()}
                    classes={{
                      root: classes.inputRoot,
                      input: classes.inputInput,
                    }}
                  />
                  <div {...getMenuProps()}>
                    {isOpen ? (
                      <Paper className={classes.paper} square>
                        {getSuggestions(suggestions, inputValue).map((suggestion, index) => {
                          const isHighlighted = highlightedIndex === index;
                          return (
                            <MenuItem
                              {...getItemProps({ item: suggestion })}
                              key={suggestion.value}
                              selected={isHighlighted}
                              component="div"
                              style={{
                                fontWeight: 400,
                                width: 400,
                              }}
                            >
                              {suggestion.label}
                            </MenuItem>
                          );
                        } 
                        )}
                      </Paper>
                    ) : null}
                  </div>
                </div>
              )}
            </Downshift>
          </div>
          <div>
            <Button variant="contained" color="default" component={'a'} href={`${window.location.protocol}//${window.location.host}`} target="_blank">Go to Website</Button>
          </div>
        </Toolbar>
      </AppBar>
    );
  }
})));
