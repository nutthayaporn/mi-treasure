import React, { Component } from 'react';
import _ from 'lodash';
import Dropzone from 'react-dropzone';
import { notify } from 'react-notify-toast';
import styled, { css } from 'styled-components';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import Button from '@material-ui/core/Button';
import AddIcon from '@material-ui/icons/Add';

import AddMediaDialog from './AddMediaDialog';

const accept = 'image/*';

const DropzoneActive = styled.div`
  position: absolute;
  z-index: -1;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background: rgba(255, 255, 255, 0.8);
  display: flex;
  border: 1px dashed #ececec;
  flex-direction: column;
  justify-content: flex-start;
  align-items: center;
  padding-top: 100px;
  box-sizing: border-box;
  color: #666;
  opacity: 0;
  transition: all 0.6s ease-out;

  ${props =>
    props.visible &&
    css`
      opacity: 1;
      z-index: 9999;
    `};
`;

const StyledButton = styled(Button)`
  ${props =>
    props.active &&
    css`
      &&& {
        background-color: lightgray;
      }
    `};
`;


export default class extends Component {
  static getDerivedStateFromProps(nextProps, state) {
    const prevProps = state.prevProps;
    const selectedValue = !_.isEqual(prevProps.value, nextProps.value) ? nextProps.value : state.selectedValue;
    return {
      prevProps: nextProps,
      selectedValue
    };
  }
  static defaultProps = {
    multiple: false,
    value: [],
    searchText: ''
  };

  static propTypes = {
    multiple: PropTypes.bool,
    value: PropTypes.arrayOf(PropTypes.string),
    searchText: PropTypes.string
  };

  state = {
    media: 0,
    dropzoneActive: false,
    showMediaDialog: false,
    prevProps: {}
  };

  // handleSelect = media => {
  //   const { multiple } = this.props;
  //   let { selectedValue } = this.state;

  //   if (multiple) {
  //     const hasMedia = _.includes(selectedValue, media.id);
  //     const updatedValue = hasMedia ? _.filter(selectedValue, value => value !== media.id) : [...selectedValue, media.id];

  //     this.setState({ selectedValue: updatedValue });
  //   } else {
  //     this.setState({ selectedValue: media.id });
  //   }
  // };
  handleSelect = mediaId => {
    this.props.onSelect(mediaId, true);
  }

  handleCloseDialog = () => this.setState({ showMediaDialog: false })

  // saveSelectedMedia = () => {
  //   const { selectedValue } = this.state;
  //   this.props.onSelect(selectedValue);
  // };

  /* Dropzone */
  onDragEnter = () => {
    this.setState({
      dropzoneActive: true
    });
  };

  onDragLeave = () => {
    this.setState({
      dropzoneActive: false
    });
  };

  onDrop = (acceptedFiles, rejectedFiles) => {
    this.setState({
      droppedFiles: acceptedFiles,
      dropzoneActive: false,
      showMediaDialog: true,
    });

    // _.map(acceptedFiles, file => {
    //   return this.props.onUploadFile(file).then(() => {
    //   });
    // });


    // const { spaceId } = this.props;
    // const storageRef = storage.ref(`cic/spaces/${spaceId}`);

    // Promise.all(
    //   _.map(acceptedFiles, uploadedFile => {
    //     const docId = uploadedFile.name;
    //     const fileRef = storageRef.child(docId);
    //     notify.show(`Uploading ${docId}`, 'success', 4000);

    //     return fileRef
    //       .put(uploadedFile)
    //       .then(snapshot => {
    //         return Promise.all([
    //           Promise.resolve(uploadedFile),
    //           Promise.resolve(snapshot.ref),
    //           Promise.resolve(snapshot.metadata),
    //           snapshot.ref.getDownloadURL()
    //         ]);
    //       })
    //       .then(([file, ref, metadata, url]) => {
    //         return { ...file, ref, metadata, url };
    //       });
    //   })
    // ).then(files => {
    //   db.collection('cic_spaces')
    //     .doc(spaceId)
    //     .collection('medias')
    //     .doc(files[0].ref.name)
    //     .set({
    //       ref: files[0].ref.toString(),
    //       title: files[0].ref.name,
    //       url: files[0].url,
    //       metadata: {
    //         updated: files[0].metadata.updated,
    //         size: files[0].metadata.size
    //       }
    //     });
    //   this.setState({ uploadedFiles: files });
    // });
  };

  handleSubmitDialog = (medias) => {
    console.log('handleSaveMedias', medias);

    _.forEach(medias, media => {
      this.props.onUploadFile(media);
      this.handleSelect(media.id);
    });

    this.setState({ showMediaDialog: false })
  }

  render() {
    const { options, searchText } = this.props;
    const { dropzoneActive, showMediaDialog, droppedFiles } = this.state;
    const visibleOptions = searchText !== '' ? _.filter(options, option => _.includes(_.toLower(option.label), _.toLower(searchText))) : options;
    // console.log('visibleOptions', visibleOptions);
    return (
      <React.Fragment>
        <div
          style={{
            display: 'flex',
            flexDirection: 'row',
            flexWrap: 'wrap'
          }}
        >
          <Dropzone
            ref={node => {
              this.dropzoneRef = node;
            }}
            disableClick
            accept={accept}
            style={{ position: 'relative' }}
            onDrop={this.onDrop}
            onDragEnter={this.onDragEnter}
            onDragLeave={this.onDragLeave}
          >
            <DropzoneActive visible={dropzoneActive}>Drop files...</DropzoneActive>
            <Button
              color="primary"
              variant="extendedFab"
              style={{ width: 150, height: 125, margin: '8px 16px' }}
              onClick={() => {
                this.dropzoneRef.open();
              }}
            >
              <AddIcon />
              Upload
            </Button>
            {_.map(visibleOptions, option => {
               const active = _.includes(this.props.value, option.value);
               return (
                <StyledButton key={option.url} onClick={() => this.handleSelect(option.value)} active={active}>
                  <div style={{ width: 150, height: 150 }}>
                    <img
                      src={option.url}
                      style={{
                        width: '100%',
                        height: '85%',
                        objectFit: 'cover'
                      }}
                      alt=""
                    />
                    <div style={{ fontSize: '12px' }}>{option.title}</div>
                  </div>
                </StyledButton>
              );
            })}
          </Dropzone>
        </div>
        <AddMediaDialog open={showMediaDialog} onClose={this.handleCloseDialog} onSave={this.handleSubmitDialog} files={droppedFiles} />
      </React.Fragment>
    );
  }
}
