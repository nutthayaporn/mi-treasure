import React from 'react';
import _ from 'lodash';
import { notify } from 'react-notify-toast';

import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import Divider from '@material-ui/core/Divider';
import TextField from '@material-ui/core/TextField';

// import {
//   saveMediaToFirestore,
// } from 'forviz-mediajs';

export default class AddMediaDialog extends React.Component {

  state = {
    items: [],
    idPrefix: '',
    prevFiles: null,
  }

  static getDerivedStateFromProps(props, state) {
    if (!_.isEqual(props.files, state.prevFiles)) {
      console.log('getDerived', props.files);
      return {
        items: _.map(props.files, file => {
          const id = _.replace(_.get(file, 'name'), /(.jpg|.png|.gif)/, '');
          return {
            id,
            title: id,
            preview: file.preview,
            tags: '',
            file,
          }
        }),
        prevFiles: props.files,
      }
    }
    return null;
  }
  
  handleChange = (name, index, value) => {
    this.setState({
      items: _.map(this.state.items, (item, itemIndex) => {
        if (itemIndex === index) {
          return {
            ...item,
            [name]: value,
          };
        }
        return item;
      }),
    });
  }

  save = () => {
    const { items } = this.state;
    // console.log('save', buildingId, items);
    
    this.props.onSave(_.map(items, item => ({
      ...item,
      tags: _.map(_.split(item.tags, ','), _.trim),
    })));

  }

  render() {
    const { open, onClose } = this.props;
    const { items } = this.state;
    return (
      <Dialog
        open={open}
        onClose={onClose}
        maxWidth="sm"
        aria-labelledby="form-dialog-title"
      >
        <DialogTitle id="form-dialog-title">Media</DialogTitle>
        <DialogContent>
          <DialogContentText></DialogContentText>
          {_.map(items, (item, index) => (
            <div key={`${item.name}-${item.index}`}>
              <Grid container spacing={24}>
                <Grid item sm={6}>
                  <img style={{ maxWidth: '100%' }} src={item.preview} alt="" />
                </Grid>
                <Grid item sm={6}>
                  <TextField
                    margin="dense"
                    id="id"
                    label="ID"
                    type="text"
                    value={item.id}
                    onChange={e => this.handleChange('id', index, e.target.value)}
                    fullWidth
                  />
                  <TextField
                    margin="dense"
                    id="title"
                    label="Title"
                    type="text"
                    value={item.title}
                    onChange={e => this.handleChange('title', index, e.target.value)}
                    fullWidth
                  />
                  <TextField
                    margin="dense"
                    id="tags"
                    label="Tags"
                    type="text"
                    value={item.tags}
                    onChange={e => this.handleChange('tags', index, e.target.value)}
                    helperText="Separate by comma"
                    fullWidth
                  />
                </Grid>
              </Grid>
              <Divider style={{ margin: '24px 0' }} />
            </div>
          ))}
        </DialogContent>
        <DialogActions>
          <Button onClick={this.save} color="primary" variant="contained">
            Save
          </Button>
        </DialogActions>
      </Dialog>
    );
    
  }
}