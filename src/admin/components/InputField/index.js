import React from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';

import Radio from '@material-ui/core/Radio';
import Checkbox from '@material-ui/core/Checkbox';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';

import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';
import Switch from '@material-ui/core/Switch';
import AreaEditor from './AreaEditor';
import FileUploader from './FileUploader';
// import FieldTextEditor from './FieldTextEditor';
import ListField from './ListField';
// import TextFieldWithSuggestion from './TextFieldWithSuggestion';

import DownshiftSingle from './Downshift/Single';
import DownShiftMultiple from './Downshift/Multiple';

import {
  TEXTFIELD,
  TEXTAREA,
  // DRAFTJS,
  RADIO,
  CHECKBOX,
  SWITCH,
  DROPDOWN,
  AUTOCOMPLETE,
  AUTOCOMPLETE_MULTIPLE,
  IMAGE,
  DATE,
  TIME,
  GALLERY,
} from './constants';

const omitProps = ['onUploadFile', 'onDeleteFile', 'list'];

const getOptionValue = option => _.get(option, 'value', option);
const getOptionLabel = option => _.get(option, 'label', option);

export default class extends React.PureComponent {

  static propTypes = {
    list: PropTypes.bool,
    multiple: PropTypes.bool,
    value: PropTypes.any,
    onChange: PropTypes.func,
    onUploadFile: PropTypes.func,
    onDeleteFile: PropTypes.func,
    renderMedia: PropTypes.func,
  }
  static defaultProps = {
    language: 'default',
    list: false,
    multiple: false,
  }

  handleCheck = (e) => {
    const value = e.target.checked;
    this.props.onChange(value);
  }

  render () {
    const { props } = this;
    switch (props.type) {
      case TEXTAREA: {
        if (props.list) {
          return (
            <ListField render={({ inputProps }) => (
              <TextField fullWidth multiline variant="outlined" {...inputProps} />
            )} />
          );
        }
        return (<TextField fullWidth multiline variant="outlined" {..._.omit(props, omitProps)} />);
      }
      case DROPDOWN:
        return (
          <TextField
            fullWidth
            select
            variant="outlined"
            {..._.omit(props, omitProps)}
            value={props.value || ''}
            InputLabelProps={{
              shrink: true,
            }}
          >
            {_.map(props.options, option =>
              <MenuItem key={getOptionValue(option)} value={getOptionValue(option)}>{getOptionLabel(option)}</MenuItem>
            )}
          </TextField>
        );
      case RADIO:
        return (
          <FormControl component="fieldset">
            <FormLabel component="legend">{props.label}</FormLabel>
            <RadioGroup
              aria-label="Gender"
              name="gender1"
              value={props.value}
              onChange={props.onChange}
            >
              {_.map(props.options, option =>
                <FormControlLabel
                  key={getOptionValue(option)}
                  value={getOptionValue(option)}
                  control={<Radio />}
                  label={getOptionLabel(option)}
                />
              )}
            </RadioGroup>
          </FormControl>
        );
      case CHECKBOX:
        return (
          <FormControl component="fieldset">
            <FormLabel component="legend">{props.label}</FormLabel>
            <RadioGroup
              aria-label={props.label}
              name="gender1"
              value={props.value}
              onChange={props.onChange}
            >
              {_.map(props.options, option =>
                <FormControlLabel
                  key={getOptionValue(option)}
                  value={getOptionValue(option)}
                  control={<Checkbox />}
                  label={getOptionLabel(option)}
                />
              )}
            </RadioGroup>
          </FormControl>
        );
      case SWITCH: {
        // console.log('Switch', props);
        return (
          <FormControlLabel
            control={
              <Switch
                checked={props.value}
                onChange={this.handleCheck}
              />
            }
            label={props.label}
          />
        );
      }
      case IMAGE: {
        // console.log('Media', props);
        return <FileUploader {...props} onUploadFile={this.props.onUploadFile} onDeleteFile={this.props.onDeleteFile} />;
      }
      case GALLERY: {
        return <FileUploader {...props} onUploadFile={this.props.onUploadFile} onDeleteFile={this.props.onDeleteFile} multiple />;
      }
      // case DRAFTJS: {
      //   return (<FieldTextEditor {...props} />);
      // }
      case DATE:
        return (
        <TextField
          fullWidth
          variant="outlined"
          {..._.omit(props, omitProps)}
          type="date"
          InputLabelProps={{
            shrink: true,
          }}
        />);
      
      case TIME:
        return (
        <TextField
          fullWidth
          variant="outlined"
          {..._.omit(props, omitProps)}
          type="time"
          InputLabelProps={{
            shrink: true,
          }}
        />);
      case 'AreaEditor':
        return (<AreaEditor {...props} />);
      
      case AUTOCOMPLETE: {
        return (
          <DownshiftSingle {...props} />
        );
      }
      case AUTOCOMPLETE_MULTIPLE: {
        return (
          <DownShiftMultiple {...props} />
        );
      }
      case TEXTFIELD:
      default: {
        if (props.list) {
          return (
            <ListField
              {..._.omit(props, omitProps)}
              render={({ inputProps }) => (
                <TextField
                  fullWidth
                  variant="outlined"
                  multiline
                  InputLabelProps={{
                    shrink: true,
                  }}
                  {...inputProps}
                />
              )}
            />
          );
        }
        return (
          <TextField
            fullWidth
            variant="outlined"
            multiline
            InputLabelProps={{
              shrink: true,
            }}
            {..._.omit(props, omitProps)}
          />
        );      
      }
    }
  }
}