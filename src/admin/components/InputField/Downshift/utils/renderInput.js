import React from 'react';
import TextField from '@material-ui/core/TextField';

export default (inputProps) => {
  const { InputProps, classes, ref, ...other } = inputProps;
  return (
    <TextField
      variant="outlined"
      InputProps={{
        inputRef: ref,
        classes: {
          root: classes.inputRoot,
          input: classes.inputInput,
        },
        ...InputProps,
      }}
      {...other}
    />
  );
}