import React from 'react';
import PropTypes from 'prop-types';
import Downshift from 'downshift';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';

import getSuggestions from './utils/getSuggestions';
import renderInput from './utils/renderInput';
import renderSuggestion from './utils/renderSuggestion';

const styles = theme => ({
  root: {
    flexGrow: 1,
    height: 250,
  },
  container: {
    flexGrow: 1,
    position: 'relative',
  },
  paper: {
    position: 'absolute',
    zIndex: 10,
    marginTop: theme.spacing.unit,
    top: 50,
    left: 0,
    right: 0,
  },
  chip: {
    margin: `${theme.spacing.unit / 2}px ${theme.spacing.unit / 4}px`,
  },
  inputRoot: {
    flexWrap: 'wrap',
  },
  inputInput: {
    width: 'auto',
    flexGrow: 1,
  },
  divider: {
    height: theme.spacing.unit * 2,
  },
});

export default withStyles(styles)(class extends React.Component {
  static defaultProps = {
    value: '',
    placeholder: `Select Item`,
  }

  static getDerivedStateFromProps (props, prevState) {
    if (props.value !== prevState.value) {
      return {
        inputValue: '',
        value: props.value,
      }
    }
    return null;
  }

  state = {
    inputValue: '',
    value: '',
  };

  handleChange = item => {
    this.setState({ value: item, inputValue: '' });
    this.props.onChange(item);
  }

  handleInputChange = event => {
    this.setState({ inputValue: event.target.value });
  };

  render() {
    const { classes, suggestions, placeholder } = this.props;
    const { inputValue, value } = this.state;
    // console.log('Single:value', value);
    // console.log('Single:inputValue', inputValue);
    return (
      <Downshift
        inputValue={inputValue}
        onChange={this.handleChange}
      >
        {({
          getInputProps,
          getItemProps,
          getMenuProps,
          highlightedIndex,
          inputValue: inputValue2,
          isOpen,
          selectedItem,
        }) => (
          <div className={classes.container}>
            {renderInput({
              fullWidth: true,
              classes,
              InputProps: getInputProps({
                value: inputValue !== '' ? inputValue : value,
                onChange: this.handleInputChange,
                placeholder,
              }),
            })}
            <div {...getMenuProps()}>
              {isOpen ? (
                <Paper className={classes.paper} square>
                  {getSuggestions(suggestions, inputValue2).map((suggestion, index) =>
                    renderSuggestion({
                      suggestion,
                      index,
                      itemProps: getItemProps({ item: suggestion.label }),
                      highlightedIndex,
                      selectedItem,
                    }),
                  )}
                </Paper>
              ) : null}
            </div>
          </div>
        )}
      </Downshift>
    );
  }
});

