import React from 'react';
import _ from 'lodash';
import { connect } from 'react-redux';
import styled from 'styled-components';
import { Link } from 'react-router-dom';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';

const StyledTableRow = styled(TableRow)`
  cursor: pointer;
  &:hover {
    background: #fafafa;
  }
`;

const mapStateToProps = (state, ownProps) => {
  const data = _.get(state, ['entities', 'facilities', 'entities', ownProps.id]);
  return {
    data,
  }
}
export default connect(mapStateToProps)(class extends React.PureComponent {
  render() {
    const { id, onClick, data } = this.props;
    const title = _.get(data, 'title');
    return (
      <StyledTableRow hover onClick={onClick}>
        {/* <TableCell style={{ color: '#AAA' }}>{id}</TableCell> */}
        <TableCell>{_.get(title, 'en', title)}</TableCell>
        <TableCell>{_.get(data, 'category', '')}</TableCell>
        <TableCell numeric>{_.get(data, 'location.lat', '')}</TableCell>
        <TableCell numeric>{_.get(data, 'location.lon', '')}</TableCell>
      </StyledTableRow>
    );
  }
});

