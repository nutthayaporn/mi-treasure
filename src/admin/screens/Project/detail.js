import React from 'react';
import _ from 'lodash';
import FirestoreDocEditor from '../../components/FirestoreDocEditor';
import { connect } from 'react-redux';

const mapStateToProps = (state, ownProps) => {
  const projectId = _.get(ownProps, 'projectId');
  const project = _.get(state, ['entities', 'projects', 'entities', projectId]);
  return {
    // medias: _.get(state, ['projects', projectId, 'medias']),
    project,
  }
};

export default connect(mapStateToProps)(class extends React.Component {
  render () {
    const { projectId, project } = this.props;
    console.log('Detail', project);
    return (
      <FirestoreDocEditor
        {...this.props}
        mediaEngine="firebase"
        mediaEngineConfig={{
          storagePath: `medias/projects/${projectId}`,
          firestorePath: `medias`
        }}
        id={projectId}
        data={project}
        // docRef={docRef}
        mapDocDataToFields={(data, id) => ({
          title: {
            label: 'Title',
            value: _.get(data, 'title.en', _.get(data, 'title', '')),
          },
          titleTH: {
            label: 'Title (ไทย)',
            value: _.get(data, 'title.th', ''),
          },
          lat: {
            label: 'Lat',
            value: _.get(data, 'location.position._lat', 13.747512)
          },
          lon: {
            label: 'Lon',
            value: _.get(data, 'location.position._long', 100.534616)
          },
          yearOpen: {
            label: 'Year Open',
            value: _.get(data, 'yearOpen', ''),
          },
          yearComplete: {
            label: 'Year Complete',
            value: _.get(data, 'yearComplete', ''),
          },
          startingPrice: {
            label: 'Starting Price',
            value: _.get(data, 'startingPrice', ''),
          },
          theAgentProjectId: {
            label: 'ProjectID',
            value: _.get(data, 'integrations.TheAgent.ProjectID', ''),
          },
          theAgentProjectCode: {
            label: 'ProjectCode',
            value: _.get(data, 'integrations.TheAgent.ProjectCode', ''),
          },
          theAgentProjectLink: {
            label: 'Link',
            value: _.get(data, 'integrations.TheAgent.ProjectLink', ''),
          },
          // Stats
          salePrice: {
            label: 'Sale Price',
            value: _.get(data, 'current.salePrice', 0),
          },
          rentPrice: {
            label: 'Rent Price',
            value: _.get(data, 'current.rentPrice', 0),
          },
          capitalGain: {
            label: 'Capital Gain',
            value: _.get(data, 'current.capitalGain', 0),
          },
          rentalYield: {
            label: 'Rental Yield',
            value: _.get(data, 'current.rentalYield', 0),
          },
        })}
        mapFieldValuesToDocData={fieldValues => {
          return {
            title: {
              en: fieldValues.title,
              th: fieldValues.titleTH,
            },
            location: {
              position: {
                _lat: _.toNumber(fieldValues.lat),
                _long: _.toNumber(fieldValues.lon),
              }
            },
            current: {
              salePrice: fieldValues.salePrice,
              rentPrice: fieldValues.rentPrice,
              capitalGain: fieldValues.capitalGain,
              rentalYield: fieldValues.rentalYield,
            },
            yearOpen: fieldValues.yearOpen,
            yearComplete: fieldValues.yearComplete,
            startingPrice: fieldValues.startingPrice,
            integrations: {
              TheAgent: {
                ProjectID: fieldValues.theAgentProjectId,
                ProjectCode: fieldValues.theAgentProjectCode,
                ProjectLink: fieldValues.theAgentProjectLink,
              }
            }
          }
        }}
        sections={[
          {
            title: 'Info',
            fields: ['title', 'titleTH', 'yearOpen', 'yearComplete', 'startingPrice']
          },
          {
            title: 'Stats',
            fields: ['salePrice', 'rentPrice', 'capitalGain', 'rentalYield'],
          },
          {
            title: 'Location',
            fields: ['lat', 'lon']
          },
          {
            title: 'Integration with TheAgent',
            fields: ['theAgentProjectId', 'theAgentProjectCode', 'theAgentProjectLink']
          },
        ]}
      />
    );
  }
});
