import React from 'react';
import { Helmet } from 'react-helmet';
import _ from 'lodash';
import { Route } from 'react-router-dom';
import { connect } from 'react-redux';

import Drawer from '@material-ui/core/Drawer';
import Button from '@material-ui/core/Button';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TablePagination from '@material-ui/core/TablePagination';

import AddIcon from '@material-ui/icons/Add';

import ProjectTableRow from '../../components/ProjectTableRow';
import PageHeader from '../../components/PageHeader';
import CreateDocButton from '../../components/CreateDocButton';

import { db } from '../../../engine';

import DetailScreen from './detail';

const mapStateToProps = state => {
  return {
    projectIds: _.map(_.get(state, 'entities.projects.entities', {}), project => project.id),
  };
}

export default connect(mapStateToProps)(class extends React.Component {
  state = {
    page: 0,
    rowsPerPage: 10,
  }

  handleSelectProject = (projectId) => {
    const { history } = this.props;
    history.push(`/admin/projects/${projectId}`);
  }

  handleCreate = (values) => {
    const { title } = values;
    const id = _.kebabCase(`${title}`);

    const docRef = db.collection('projects').doc(id);
    docRef.set({
      title: {
        en: title,
      },
    }).then(() => {
      const { match, history } = this.props;
      history.push(`${match.url}/${id}`);  
    });
  }

  handleChangePage = (event, page) => {
    this.setState({ page });
  }

  handleChangeRowsPerPage = event => {
    this.setState({ rowsPerPage: event.target.value });
  }

  render() {
    const { match, history, projectIds } = this.props;
    const listScreenUrl = match.url;
    const { rowsPerPage, page } = this.state;
    const visibleIds = projectIds.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage);

    return (
      <div>
        <Helmet>
          <title>Projects | MI Treasure</title>
        </Helmet>
        <PageHeader
          title="Projects"
          renderActionButtons={() =>
            <CreateDocButton
              label="Facility"
              fields={[
                { type: 'TextField', name: 'title', label: 'Title', required: true },
              ]}
              renderButton={() => <Button variant="extendedFab" color="primary"><AddIcon /> Add Project</Button>}
              onCreate={this.handleCreate}
            />
          }
        />
        <Table>
          <TableHead>
            <TableRow>
              <TableCell>Title</TableCell>
              <TableCell>TheAgent ID</TableCell>
              <TableCell>Sale</TableCell>
              <TableCell>Rent</TableCell>
              <TableCell>TheAgent</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {_.map(visibleIds, (projectId, index) =>
              <ProjectTableRow
                key={`${projectId}-${index}`}
                id={projectId}
                onClick={e => this.handleSelectProject(projectId)}
              />
            )}
          </TableBody>
        </Table>
        <TablePagination
          rowsPerPageOptions={[5, 10, 25]}
          component="div"
          count={projectIds.length}
          rowsPerPage={rowsPerPage}
          page={page}
          backIconButtonProps={{
            'aria-label': 'Previous Page',
          }}
          nextIconButtonProps={{
            'aria-label': 'Next Page',
          }}
          onChangePage={this.handleChangePage}
          onChangeRowsPerPage={this.handleChangeRowsPerPage}
        />
        <Route
          path={`${match.url}/:projectId`}
          children={({ match, ...rest }) => {
            const projectId = _.get(match, 'params.projectId');
            console.log('projectId', projectId);
            // const docRef = !_.isEmpty(projectId) ? db.collection('projects').doc(projectId) : null;
            const open = projectId !== undefined;
            return (
              <Drawer anchor="right" open={open} onClose={() => history.push(listScreenUrl)}>
                <DetailScreen
                  width="700px"
                  {...rest}
                  projectId={projectId}
                  // docRef={docRef}
                  onClose={() => {
                    history.push(listScreenUrl);
                  }}
                />
              </Drawer>
            );
          }}
        />
      </div>
    );
  }
});
