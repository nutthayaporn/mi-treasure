import React from 'react';
import _ from 'lodash';
import FirestoreDocEditor from '../../components/FirestoreDocEditor';
import { connect } from 'react-redux';

import { categoryOptions } from './constants';


const mapStateToProps = (state, ownProps) => {
  const facilityId = _.get(ownProps, 'facilityId');
  const facility = _.get(state, ['entities', 'facilities', 'entities', facilityId]);
  
  return {
    medias: _.get(state, ['projects', facilityId, 'medias']),
    facility,
  }
};

export default connect(mapStateToProps)((props) => {
  const { facilityId, facility } = props;
  return (
    <FirestoreDocEditor
      {...props}
      mediaEngine="firebase"
      mediaEngineConfig={{
        storagePath: `medias/facilities`,
        firestorePath: `medias`
      }}
      id={facilityId}
      data={facility}
      // docRef={props.docRef}
      mapDocDataToFields={(data, id) => ({
        title: {
          label: "Title",
          value: _.get(data, "title.en", _.get(data, "title", ""))
        },
        titleTH: {
          label: "Title (ไทย)",
          value: _.get(data, "title.th", "")
        },
        category: {
          label: "Category",
          type: 'Dropdown',
          value: _.get(data, "category", ""),
          options: categoryOptions,
        },
        lat: {
          label: "Lat",
          value: _.get(data, "location.lat", 13.747512)
        },
        lon: {
          label: "Lon",
          value: _.get(data, "location.lon", 100.534616)
        }
      })}
      mapFieldValuesToDocData={fieldValues => {
        return {
          title: {
            en: fieldValues.title,
            th: fieldValues.titleTH
          },
          location: {
            lat: fieldValues.lat,
            lon: fieldValues.lon
          },
          category: fieldValues.category,
        }
      }}
      sections={[
        {
          title: 'Info',
          fields: ['title', 'titleTH', 'category']
        },
        {
          title: 'Location',
          fields: ['lat', 'lon']
        },
      ]}
    />
  );
});