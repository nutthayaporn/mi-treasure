import React from 'react';
import { Route } from 'react-router-dom';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import _ from 'lodash';
import styled from 'styled-components';
import Button from '@material-ui/core/Button';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TablePagination from '@material-ui/core/TablePagination';

import AddIcon from '@material-ui/icons/Add';

// import Modal from '@material-ui/core/Modal';
import Drawer from '@material-ui/core/Drawer';

import PageHeader from '../../components/PageHeader';
import CreateDocButton from '../../components/CreateDocButton';
import FacilityTableRow from '../../components/FacilityTableRow';

import DetailScreen from './detail';

import { db } from '../../../engine';

import { categoryOptions } from './constants';

const BlockContent = styled.div`
  min-height: 100%;
`;

const mapStateToProps = state => {
  return {
    facilityIds: _.compact(_.map(_.get(state, 'entities.facilities.entities', []), facility => facility.id)),
  }
}
export default connect(mapStateToProps)(class extends React.PureComponent {
  state = {
    page: 0,
    rowsPerPage: 10,
  }

  handleSelectFacility = (facilityId) => {
    const { match, history } = this.props;
    history.push(`${match.url}/${facilityId}`);
  }

  handleSave = (facilityId) => {
    const { history } = this.props;
    history.push(`/admin/facilities/${facilityId}`);
    this.handleCloseModal();
  }

  handleCreate = (values) => {
    const { title, category } = values;
    const id = _.kebabCase(`${title}`);

    const docRef = db.collection('facilities').doc(id);
    docRef.set({
      title: {
        en: title,
      },
      category: category,
    }).then(() => {
      const { match, history } = this.props;
      history.push(`${match.url}/${id}`);  
    });

  }

  render() {
    const { match, history, facilityIds } = this.props;
    const listScreenUrl = match.url;

    const { rowsPerPage, page } = this.state;
    const visibleIds = facilityIds.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage);

    // const { facilities } = this.state;
    return (
      <BlockContent>
        <Helmet>
          <title>Facilities | MI Treasure</title>
        </Helmet>
        <PageHeader
          title="Facilities"
          renderActionButtons={() =>
            <CreateDocButton
              label="Facility"
              fields={[
                { type: 'TextField', name: 'title', label: 'Title', required: true },
                { type: 'Dropdown', name: 'category', label: 'Category', options: categoryOptions },
              ]}
              renderButton={() => <Button variant="extendedFab" color="primary"><AddIcon /> Add Facility</Button>}
              onCreate={this.handleCreate}
            />
          }
        />
        <Table>
          <TableHead>
            <TableRow>
              {/* <TableCell>ID</TableCell> */}
              <TableCell>Title</TableCell>
              <TableCell>TYPE</TableCell>
              <TableCell>LAT</TableCell>
              <TableCell>LON</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {_.map(visibleIds, facilityId =>
              <FacilityTableRow
                key={facilityId}
                id={facilityId}
                onClick={e => this.handleSelectFacility(facilityId)}
              />
            )}
          </TableBody>
        </Table>
        <TablePagination
          rowsPerPageOptions={[5, 10, 25]}
          component="div"
          count={facilityIds.length}
          rowsPerPage={rowsPerPage}
          page={page}
          backIconButtonProps={{
            'aria-label': 'Previous Page',
          }}
          nextIconButtonProps={{
            'aria-label': 'Next Page',
          }}
          onChangePage={this.handleChangePage}
          onChangeRowsPerPage={this.handleChangeRowsPerPage}
        />
        {/* <Modal
          open={this.state.openModel}
          onClose={this.handleCloseModal}
        >
        </Modal> */}
        <Route
          path={`${match.url}/:facilityId`}
          children={({ match, ...rest }) => {
            const facilityId = _.get(match, 'params.facilityId');
            // const docRef = !_.isEmpty(facilityId) ? db.collection('facilities').doc(facilityId) : null;
            const open = facilityId !== undefined;
            return (
              <Drawer anchor="right" open={open} onClose={() => history.push(listScreenUrl)}>
                <DetailScreen
                  width="700px"
                  {...rest}
                  facilityId={facilityId}
                  // docRef={docRef}
                  onClose={() => {
                    history.push(listScreenUrl);
                  }}
                />
              </Drawer>
            );
          }}
        />
      </BlockContent>
    );
  }
});
