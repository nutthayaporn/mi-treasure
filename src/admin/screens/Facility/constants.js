export const categoryOptions = [
  { value: 'shopping', label: 'Shopping' },
  { value: 'office', label: 'Office' },
  { value: 'education', label: 'Education' },
  { value: 'hospital', label: 'Hospital' },
  { value: 'park', label: 'Park' },
  { value: 'embassy', label: 'Embassy' },
];