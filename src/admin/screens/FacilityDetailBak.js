import React from "react";
import styled from "styled-components";
import _ from "lodash";
import { notify } from "../../components/Notification";

import Typography from "@material-ui/core/Typography";
import Paper from "@material-ui/core/Paper";
import Divider from "@material-ui/core/Divider";
import Input from "@material-ui/core/Input";

import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import Button from "@material-ui/core/Button";

import Map from "../../components/Map";

// import { GoogleMap, Marker } from "react-google-maps";

import { db } from "../../engine";

// import { updateFacility, updateFacilityHistory } from '../api/facility';

const DRAWER_WIDTH = 600;

const PaperContent = styled.div`
  padding: 40px;
  box-sizing: border-box;
`;

const Heading = styled.div`
  color: #5a5a5a;
  font-weight: bold;
  font-size: 0.875rem;
`;

const SeconaryHeading = styled.div`
  color: #787878;
  font-size: 0.875rem;
`;

const Section = styled.section`
  margin: 25px 0;
`;

const ActionBar = styled.div`
  box-sizing: border-box;
  padding: 20px;
  right: 0;
  bottom: 0;
  width: 100%;
  background: #fafafa;
  border-top: 1px solid #ececec;
  display: flex;
  justify-content: flex-end;
`;

const mapDataToFieldValues = data => {
  return {
    title: {
      label: "Title",
      value: _.get(data, "title.en", _.get(data, "title", ""))
    },
    titleTH: {
      label: "Title (ไทย)",
      value: _.get(data, "title.th", "")
    },
    type: {
      label: "Type",
      value: _.get(data, "type", "")
    },
    lat: {
      label: "Lat",
      value: _.get(data, "location.lat", 13.747512)
    },
    lon: {
      label: "Lon",
      value: _.get(data, "location.lon", 100.534616)
    }
  };
};

const mapFieldValuesToData = fieldValues => {
  return {
    title: {
      en: fieldValues.title,
      th: fieldValues.titleTH
    },
    location: {
      lat: fieldValues.lat,
      lon: fieldValues.lon
    },
    type: fieldValues.type
  };
};

const BlockContent = styled.div`
  min-height: 100%;
`;

export default class extends React.PureComponent {
  static defaultProps = {
    id: "teemp"
  };

  state = {
    fields: mapDataToFieldValues(),
    reRenderMap: 0,
    typeOfFacility: []
  };

  loadFacility = facilityId => {
    if (facilityId) {
      this.unsubscribeFacility = db
        .collection("facilities")
        .doc(facilityId)
        .onSnapshot(doc => {
          this.setState({
            reRenderMap: this.state.reRenderMap + 1,
            fields: mapDataToFieldValues(doc.data())
          });
        });
      db.collection("typeOfFacility").onSnapshot(querySnapshot => {
        const typeOfFacility = _.map(querySnapshot.docs, doc => {
          return doc.data();
        });
        this.setState({
          typeOfFacility
        });
      });
    }
  };

  componentDidMount() {
    this.loadFacility(this.props.facilityId);
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.props.facilityId !== prevProps.facilityId) {
      this.loadFacility(this.props.facilityId);
    }
  }

  componentWillUnmount() {
    // this.unsubscribeFacility();
    // this.unsubscribeFacilityHistory();
  }

  save = e => {
    const { facilityId, history } = this.props;
    e.preventDefault();
    const fieldValues = _.mapValues(this.state.fields, field => field.value);
    const data = mapFieldValuesToData(fieldValues);

    if (facilityId === '0') {
      db.collection("facilities").add(data);
    } else {
      db.collection("facilities")
        .doc(facilityId)
        .set(data, { merge: true });
    }
    notify.show("Facility Saved", "success");

    history.push(`/admin/facilities`);
  };

  handleInputChange = (name, value) => {
    this.setState({
      fields: {
        ...this.state.fields,
        [name]: {
          ...this.state.fields[name],
          value
        }
      }
    });
  };

  renderField = (fieldName, typeOfInput, data = []) => {
    const field = _.get(this.state, `fields.${fieldName}`, "");
    const { label, value } = field;
    const showValue = value;
    if (typeOfInput === "select") {
      return (
        <div
          style={{
            display: "flex",
            padding: "6px 0",
            justifyContent: "space-between",
            alignItems: "center"
          }}
        >
          <div style={{ flexBasis: 200 }}>
            <Typography variant="caption">{label}</Typography>
          </div>
          <div style={{ flex: 1 }}>
            <FormControl>
              <InputLabel htmlFor="demo-controlled-open-select">
                {label}
              </InputLabel>
              <Select
                value={value}
                onChange={e =>
                  this.handleInputChange(fieldName, e.target.value)
                }
                inputProps={{
                  name: "type",
                  id: "typeOfFacility"
                }}
              >
                <MenuItem value={0}>
                  <em>Select Type</em>
                </MenuItem>
                {_.map(data, value => {
                  return (
                    <MenuItem value={value.id}>
                      {_.get(value, "title.en", "")}
                    </MenuItem>
                  );
                })}
              </Select>
            </FormControl>
          </div>
        </div>
      );
    }
    return (
      <div
        style={{
          display: "flex",
          padding: "6px 0",
          justifyContent: "space-between",
          alignItems: "center"
        }}
      >
        <div style={{ flexBasis: 200 }}>
          <Typography variant="caption">{label}</Typography>
        </div>
        <div style={{ flex: 1 }}>
          <Input
            fullWidth
            value={showValue}
            style={{ boxShadow: "none" }}
            onChange={e => this.handleInputChange(fieldName, e.target.value)}
          />
        </div>
      </div>
    );
  };

  handleClickMap = event => {
    const lat = event.latLng.lat();
    const lng = event.latLng.lng();
    this.handleInputChange("lat", lat);
    this.handleInputChange("lon", lng);
  };

  render() {
    const { history, facilityId } = this.props;
    console.log(facilityId);
    const { fields, reRenderMap, typeOfFacility } = this.state;
    const lat = _.toNumber(_.get(this.state, "fields.lat.value", 13.747512));
    const lon = _.toNumber(_.get(this.state, "fields.lon.value", 100.534616));
    return (
      <BlockContent>
        <Paper
          elevation={0}
          style={{ width: "100%", padding: "40px 0", boxSizing: "border-box" }}
        >
          <PaperContent>
            <Typography variant="display1">
              {_.get(fields, "title.value", "")}
            </Typography>
            <Section>
              <Typography variant="subheading">Info</Typography>
              <div>
                {this.renderField("title")}
                {this.renderField("titleTH")}
                {this.renderField("type", "select", typeOfFacility)}
              </div>
            </Section>
            <Divider />
            <Section>
              <Typography variant="subheading">Location</Typography>
              <div style={{ height: 500 }}>
                <Map
                  key={`update-${reRenderMap}`}
                  innerRef={c => (this.map = c)}
                  googleMapURL={`https://maps.googleapis.com/maps/api/js?key=${
                    process.env.REACT_APP_GOOGLE_APIKEY_BROWSER
                  }&v=3.exp&libraries=geometry,drawing,places,visualization`}
                  defaultCenter={{ lat, lng: lon }}
                  defaultZoom={15}
                  onZoomChanged={this.handleZoomChange}
                  onClick={this.handleClickMap}
                  options={{
                    mapTypeControl: false,
                    fullscreenControl: false,
                    zoomControl: true,
                    streetViewControl: false
                  }}
                  loadingElement={<div style={{ height: `100%` }} />}
                  containerElement={<div style={{ height: `100%` }} />}
                  mapElement={<div id="map" style={{ height: `100%` }} />}
                />
              </div>
              <div>
                {this.renderField("lat")}
                {this.renderField("lon")}
              </div>
            </Section>
          </PaperContent>
        </Paper>
        <ActionBar>
          <Button onClick={history.goBack}>Cancel</Button>
          <Button color="primary" onClick={this.save}>
            Save
          </Button>
        </ActionBar>
        {/* 
          <Map
            defaultZoom={8}
            defaultCenter={{ lat: -34.397, lng: 150.644 }}
          /> */}
      </BlockContent>
    );
  }
}
