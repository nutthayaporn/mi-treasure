export const addToCompare = (id) => ({ type: 'COMPARE/ADD', id });
export const removeFromCompare = (id) => ({ type: 'COMPARE/REMOVE', id });

export const addAllToCompare = (ids) => ({ type: 'COMPARE/ADD/ALL', ids });
export const removeAllFromCompare = () => ({ type: 'COMPARE/REMOVE/ALL' });
