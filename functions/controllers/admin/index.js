const fetch = require('isomorphic-fetch');
const _ = require('lodash');
const express = require('express');
const cors = require('cors')({ origin: true });
const admin = require('../../config');
const iam = require('../../middleware/iam');
const getProjectStat = require('../../utils/getProjectStat');

const app = express();

app.use(cors);
const db = admin.firestore();

/* 
* User
* =============== */

const listAllUsers = (nextPageToken) => {
  // List batch of users, 1000 at a time.
  return admin.auth().listUsers(1000, nextPageToken).then((listUsersResult) => {
    return listUsersResult.users;
  }).catch((error) => {
    console.log("Error listing users:", error);
    res.json({ error });
  });
}

app.get('/users', iam('mi-treasure.admin'), (req, res) => {
 listAllUsers()
   .then(data => {
     return res.json({
       status: 'success',
       data
     });
   })
   .catch(error => {
     res.status(500).json({ error });
   })
});

app.post('/users', iam('mi-treasure.admin'), (req, res) => {
  const { email, password, displayName, role } = req.body;
 
  // Find User
  return admin.auth().createUser({ email, password, displayName })
    .then(userRecord => {
      admin.auth().setCustomUserClaims(userRecord.uid, {
        roles: [role]
      }).then(() => {
        return res.status(201).json({
          status: 'success',
          data: Object.assign({}, userRecord),
        });
      }).catch(error => {
        console.log('error1', error);
        return res.status(500).json({
          status: 'error',
          error,
        });
      });
    }).catch(error => {
      console.log('error2', error);
      return res.status(500).json({
        status: 'error',
        error,
      });
    });
});
// 
app.put('/users/:uid', iam('mi-treasure.admin'), (req, res) => {
  const uid = req.params.uid;
  const { body } = req;
  const { roles } = body;

  admin.auth().setCustomUserClaims(uid, {
    roles: roles
  }).then(() => {
    return admin.auth().getUser(uid).then((userRecord) => {
      return res.status(201).json({
              status: 'success',
              data: Object.assign({}, userRecord.customClaims),
            });
    });
  }).catch(error => {
    console.log('error1', error);
    return res.status(500).json({
      status: 'error',
      error,
    });
  });

  // admin.auth().updateUser(uid, body)
  //   .then(userRecord => {
  //     console.log(userRecord);
  //     // return admin.firestore().collection('users').doc(uid).get()
  //     //   .then(info => {
  //     //     return res.status(200).json({
  //     //       status: 'success',
  //     //       data: Object.assign({}, userRecord, info),
  //     //     })
  //     //   })
  //   })
  //   .catch((error) => {
  //     return res.status(500).json({
  //       status: 'error',
  //       error,
  //     });
  //   });
});

app.delete('/users/:uid', iam('mi-treasure.admin'), (req, res) => {
  const uid = req.params.uid;
  
    admin.auth().deleteUser(uid)
    .then(userRecord => {
      return admin.firestore().collection('users').doc(uid).delete();
    })
    .then(() => {
      return res.status(200).json({
        status: 'success',
      });
    }).catch(error => {
      return res.status(500).json({
        status: 'error',
        error,
      });
    });
});


/* 
 * Projects
 * =============== */

const calculateRentPriceFromSources = (sources) => {
  return sources[0].rentPrice;
}

const calculateSalePriceFromSources = (sources) => {
  return sources[0].salePrice;
}

// Update Project
app.put('/projects/:projectId', iam('mi-treasure.admin'), (req, res) => {
  const { projectId } = req.params;
  const data = req.body;
  db.collection('projects').doc(projectId).set(data, { merge: true })
    .then(() => {
      return res.status(200).json({ status: 'success', data });
    })
    .catch(error => {
      return res.status(500).json({ error });
    })
});

// Update Project History
app.put('/projects/:projectId/history/:time/sources/:sourceId', iam('mi-treasure.admin'), (req, res) => {
  const { projectId, time, sourceId } = req.params;
  const { salePrice, rentPrice, remark } = req.body;

  const projectRef = db.collection('projects').doc(projectId);

  return db.runTransaction((transaction) => {
    // This code may get re-run multiple times if there are conflicts.
    let projectData = undefined;
    return transaction.get(projectRef).then((projectDoc) => {
      projectData = projectDoc.data();
      return transaction.get(projectRef.collection('history'))
    })
    .then(querySnapshot => _.map(querySnapshot.docs, doc => Object.assign({}, doc.data(), { id: doc.id })))
    .then((historyItems) => {
      const historyItem = _.find(historyItems, item => item.id === time);
      const { sources } = historyItem;

      const updatedSources = _.map(sources, src => {
        if (src.source === sourceId) {
          return Object.assign({}, src, {
            salePrice,
            rentPrice,
            remark,
          });
        }
        return src;
      });

      const updatedHistoryItem = {
        sources: updatedSources,
        rentPrice: calculateRentPriceFromSources(updatedSources),
        salePrice: calculateSalePriceFromSources(updatedSources),
        updatedAt: admin.firestore.FieldValue.serverTimestamp(),
      };

      transaction.update(projectRef.collection('history').doc(time), updatedHistoryItem);
      transaction.update(projectRef, { test: 'Wonder Woman' });

      return _.map(historyItems, item => {
        if (item.id === historyItem.id) return Object.assign({}, item, updatedHistoryItem);
        return item;
      });
    })
    .then((updatedHistoryItems) => {
      const projectStat = getProjectStat(Object.assign({}, projectData, {
        history: updatedHistoryItems
      }));
      console.log('stat', projectStat);
      transaction.update(projectRef, {
        current: projectStat
      });
      return projectData;
    });
  }).then(() => {
    return res.status(200).json({ status: 'success' });
  }).catch((error) => {
    console.log('error', error);
    return res.status(500).json({ status: 'error', error });
  });

});


module.exports = app;
