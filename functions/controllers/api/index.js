const fetch = require('isomorphic-fetch');
const express = require('express');
const cors = require('cors')({ origin: true });
const admin = require('../../config');

const app = express();

app.use(cors);

// const admin = require('firebase-admin');
// admin.initializeApp();

const key = 'AIzaSyDdd7N3FCJUB609zDzsk0H-Ko76NfdX-QU';

const fetchPlaceByType = (location, radius, type) => {
  return fetch(`https://maps.googleapis.com/maps/api/place/nearbysearch/json?key=${key}&location=${location}&radius=${radius}&type=${type}`)
    .then(response => response.json())
    .then(response => {
      return response.results;
    });
}

app.get('/projects', (req, res) => {
  const { location, radius } = req.query; 
});

// =========================== //

app.get('/place/nearbysearch', (req, res) => {
  const { location, key, radius } = req.query;

  const placePromises = [
    fetchPlaceByType(location, radius, 'shopping_mall'),
    fetchPlaceByType(location, radius, 'supermarket'),
    fetchPlaceByType(location, radius, 'hospital'),
    fetchPlaceByType(location, radius, 'park'),
    fetchPlaceByType(location, radius, 'school'),
  ];

  Promise.all(placePromises)
    .then(places => {
      // console.log('places', places);
      return res.status(200).json({
        data: {
          shopping: [
            ...places[0],
            ...places[1],
          ],
          hospital: places[2],
          park: places[3],
          school: places[4],
        }
      });
    })
    .catch(error => {
      return res.status(500).json(error);
    })
});

module.exports = app;