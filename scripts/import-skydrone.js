const admin = require("firebase-admin");

const fs = require('fs');
const csv = require("fast-csv");
const _ = require('lodash');

const serviceAccount = require('./mi-treasure-firebase-adminsdk-ipk3u-a01ae807b0.json');

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://mi-treasure.firebaseio.com"
});

const db = admin.firestore();

const knex = require('knex')({
  client: 'mssql',
  connection: {
    host : '203.151.59.23',
    user : 'anandaagentts',
    password : 'T@gentTS17',
    database : 'TenancyService_UAT'
  }
});

const runProcess = async () => {
  const stations = await knex.select('*').whereIn('transportation_id', [1, 2]).where('enable', 1).from('Station');
  console.log('stations', stations);
  const data = _.map(stations, station => {
    return {
      code: `BTS.${station.initial}`,
      video: `https://www.theagent.co.th/detail/Station/${station.id}/${station.video_file}`,
    }
  });

  _.forEach(data, station => {
    db.collection('locations').doc(station.code).update({
      skydrone_video: station.video,
    });
  })
  // const PROJECT_COLLECTION = 'projects';
}

runProcess();