const admin = require("firebase-admin");

const fs = require('fs');
const csv = require("fast-csv");
const _ = require('lodash');

const runProcess = async () => {
  const serviceAccount = require('./mi-treasure-firebase-adminsdk-ipk3u-a01ae807b0.json');
  
  const PROJECT_COLLECTION = 'locations';
  
  admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://mi-treasure.firebaseio.com"
  });
  
  const db = admin.firestore();
  
  var stream = fs.createReadStream("./upload/MI Treasure - Locations.csv");
   
  var csvStream = csv.fromStream(stream, { headers: true, ignoreEmpty: true })
    .transform(function(data){
      const id = data.ID;
      return {
        id,
        location: {
          position: {
            _lat: _.toNumber(_.trim(_.get(data, 'Lat'))),
            _long: _.toNumber(_.trim(_.get(data, 'Long'))),
          }
        },
        title: {
          en: _.trim(_.get(data, 'TitleEN')),
          th: _.trim(_.get(data, 'TitleTH')),
        },
        active:_.get(data, 'active', 0),
        type: _.trim(_.get(data, 'Type', '')),
      }
    })
    .on("data", function(data){
      // console.log(data);
      const info = {...data};
      console.log('info', info);
      db.collection(PROJECT_COLLECTION)
        .doc(info.id)
        .set(info)
        .then(() => {
          console.log("Document written with ID: ", info.id);
        })
        .catch(function(error) {
            console.error("Error adding document: ", error);
        });
    
      // console.log(json);
    })
    .on("end", function(){
      console.log("done");
    });
};

runProcess();
