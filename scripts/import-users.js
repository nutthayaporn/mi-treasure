const admin = require("firebase-admin");
const fs = require('fs');
const csv = require("fast-csv");
const _ = require('lodash');

var serviceAccount = require('../credentials/mi-treasure-production.json');

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: 'https://mi-treasure-production.firebaseio.com'
});

const stream = fs.createReadStream('./temp/mi-users.csv');
const result = [];

const getUser = userData => {
  return new Promise((resolve, reject) => {
    // if (userData.email === 'annop@theagent.co.th') {
      console.log('getUser', userData.email);
      admin.auth().getUserByEmail(userData.email)
        .then(userRecord => {
          if (!_.isEqual(_.pick(userRecord, ['email', 'phoneNumber', 'displayName']), _.pick(userData, ['email', 'phoneNumber', 'displayName']))) { 
            return admin.auth().updateUser(userRecord.uid, {
              email: userData.email,
              emailVerified: false,
              // phoneNumber: userData.phoneNumber,
              password: userData.password,
              displayName: userData.displayName,
              disabled: false
            })
          } else {
            return userRecord;
          }
        }).then(userRecord => resolve(userRecord))
        .catch(error => {
          switch (error.code) {
            case 'auth/user-not-found':
              admin.auth().createUser({
                email: userData.email,
                emailVerified: false,
                phoneNumber: userData.phoneNumber,
                password: userData.password,
                displayName: userData.displayName,
                disabled: false
              })
              .then(userRecord => resolve(userRecord))
              .catch(error => {
                console.log('createError', userData.email, error.code);
                reject(error);
              });
              break;
            default:
              console.log('error code', userData.email, error.code);
              reject(error);
              break;
          }
        });
    // } else {
    //   reject();
    // }
  });
}

const importUserTask = (userData) => {
  return new Promise((resolve, reject) => {
    getUser(userData)
      .then(userRecord => {
        // Update User Info
        console.log(userData.email, ': Update Firestore');
        return admin.firestore().collection('users').doc(userRecord.uid).set(userData.data).then(() => userRecord);
      })
      .then(userRecord => {
        // Set Custom claims
        return admin.auth().setCustomUserClaims(userRecord.uid, {
          roles: [userData.role]
        }).then(() => userRecord);
      })
      .then(resolve)
      .catch(reject);
  });
}

const csvStream = csv.fromStream(stream, { headers: true, ignoreEmpty: true })
  .transform(function(data){
    return {
      role: _.get(data, 'No.') === 'Admin' ? 'mi-treasure.admin' : 'mi-treasure.sale',
      email: _.get(data, 'E-mail'),
      password: _.get(data, 'Password'),
      displayName: _.get(data, 'Name(English)'),
      phoneNumber: _.replace(_.replace(_.get(data, 'Mobile'), /-/g, ''), /^0/, '+66').substring(0, 12),
      data: {
        nickName: _.get(data, 'Nickname'),
        fullName: _.get(data, 'Name(English)'),
        fullNameTH: _.get(data, 'Name(Thai)'),
        phoneNumber: _.replace(_.replace(_.get(data, 'Mobile'), /-/g, ''), /^0/, '+66'),
        company: 'The Agent',
        position: _.get(data, 'Position'),
        dept: _.get(data, 'Dept'),
      }
    };
  })
  .on("data", (row) => {
    result.push(importUserTask(row));
  })
  .on("end", async () => {
    
    try {
      // const chunked = _.chunk(result, 10);
      // console.log("done", chunked);
      const promiseResult = await result.reduce((promiseChain, currentTask) => {
        return promiseChain.then(chainResults =>
            currentTask.then(currentResult =>
                [ ...chainResults, currentResult ]
            )
        );
      }, Promise.resolve([]))
      
      console.log('promiseResult', promiseResult);
    } catch (error) {
      console.log('end', error);
    }
  });

