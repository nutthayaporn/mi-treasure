const fs = require('fs');
const csv = require("fast-csv");
const _ = require('lodash');

var admin = require("firebase-admin");

var serviceAccount = require("./mi-treasure-firebase-adminsdk-ipk3u-241a5f5787.json");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),  // that credential from firebase-admin (server connect to server)
  databaseURL: "https://mi-treasure.firebaseio.com"
});

console.log('hello');
const csvFile = './MI Treasure Facility - Sheet1.csv';

const stream = fs.createReadStream(csvFile);


// load csvdata from file and then transform to the data that our need.
// after that, add on database (admin.firestore()) firebase
const csvStream = csv.fromStream(stream, { headers: true, ignoreEmpty: true })
  .transform(function(data){
    const latData = _.split(data.GPS, ',')[0];
    const lonData = _.split(data.GPS, ',')[1];
    return {
      id: _.kebabCase(data.Name),
      title: {
        en: data.Name,
        th: data.Name,
      },
      location: {
        lat: latData,
        lon: lonData,
      },
      category: data.category
    }
  })
  .on("data", function(data){
    
    if (data.id !== '') { // data.id === 'grand-bangsue-interchange' && 
        console.log('data', data);
        admin.firestore().collection('facilities').doc(data.id) // doc(data.id) should be unique infomation
        .set({  // find same data.id and then overlap
          ...data,
          meta: {
            updatedAt: admin.firestore.FieldValue.serverTimestamp(),
          },
        })
        .then(() => {
          console.log("Document written with ID: ", data.id);
        })
        .catch(function(error) {
          console.error("Error adding document: ", error);
        });
    }
  
    // console.log(json);
  })
  .on("end", function(){
    console.log("done");
  });